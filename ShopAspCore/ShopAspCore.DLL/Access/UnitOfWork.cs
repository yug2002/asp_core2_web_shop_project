﻿using ShopAspCore.DLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using ShopAspCore.DLL.Entities;
using System.Threading.Tasks;
using ShopAspCore.DLL.DbRepository;
using ShopAspCore.DLL.Context;
using Microsoft.AspNetCore.Identity;

namespace ShopAspCore.DLL.Access
{
    public class UnitOfWork : IUnitOfWork
    {   

        private ShopContext context;

        public UnitOfWork(ShopContext context)
        {
            this.context = context;
        }
        public IRepository<Good> Goods => new GoodRepository(context);

        public IRepository<Brand> Brands => new BrandRepository(context);        

        public IRepository<Category> Categories => new CategoryRepository(context);

        public IRepository<Basket> Baskets => new BasketRepository(context);

        public IRepository<UserProfile> UserProfiles => new UserProfileRepository(context);

        public IEnumerable<IdentityRole> Roles => context.Roles;        

        public async Task<int> SaveAsync()
        {
           return await Task.Run(()=> context.SaveChanges());
        }

        #region IDisposable Support
        private bool disposedValue = false; // Для определения избыточных вызовов

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: освободить управляемое состояние (управляемые объекты).
                }

                // TODO: освободить неуправляемые ресурсы (неуправляемые объекты) и переопределить ниже метод завершения.
                // TODO: задать большим полям значение NULL.

                disposedValue = true;
            }
        }

        // TODO: переопределить метод завершения, только если Dispose(bool disposing) выше включает код для освобождения неуправляемых ресурсов.
        // ~UnitOfWork() {
        //   // Не изменяйте этот код. Разместите код очистки выше, в методе Dispose(bool disposing).
        //   Dispose(false);
        // }

        // Этот код добавлен для правильной реализации шаблона высвобождаемого класса.
        public void Dispose()
        {
            // Не изменяйте этот код. Разместите код очистки выше, в методе Dispose(bool disposing).
            Dispose(true);
            // TODO: раскомментировать следующую строку, если метод завершения переопределен выше.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
