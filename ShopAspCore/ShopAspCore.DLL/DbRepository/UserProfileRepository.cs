﻿using ShopAspCore.DLL.Entities;
using ShopAspCore.DLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using ShopAspCore.DLL.Context;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Linq.Expressions;

namespace ShopAspCore.DLL.DbRepository
{
    public class UserProfileRepository : IRepository<UserProfile>
    {
        private ShopContext context;

        public UserProfileRepository(ShopContext context)
        {
            this.context = context;
        }
        public async Task Create(UserProfile item)
        {
            await context.UserProfiles.AddAsync(item);
        }

        public async Task Delete(int Id)
        {
            var item = context.UserProfiles.FirstOrDefault(xx => xx.Id == Id.ToString());
            await Task.Run(() => context.UserProfiles.Remove(item));
        }

        public async Task<IQueryable<UserProfile>> Find(Expression<Func<UserProfile, bool>> predicate)
        {
            return await Task.Run(() => context.UserProfiles.Include(xx=>xx.Basket).Where(predicate).AsQueryable());
        }

        public Task<UserProfile> Get(int Id)
        {
            throw new NotImplementedException();
        }

        public async Task<UserProfile> Get(string Id)
        {
            return await Task.Run(() => context.UserProfiles.Include(xx=>xx.Basket).FirstOrDefault(xx => xx.Id == Id));            
        }

        public async Task<IQueryable<UserProfile>> GetAll()
        {
            return await Task.Run(() => context.UserProfiles.AsQueryable());
        }        

        public async Task Update(UserProfile item)
        {
            var u = await Get(item.Id);
           
            //u.Basket = item.Basket;
            await Task.Run(() => context.Entry(u).State = EntityState.Modified);
        }
    }
}
