﻿using ShopAspCore.DLL.Entities;
using ShopAspCore.DLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using ShopAspCore.DLL.Context;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Linq.Expressions;

namespace ShopAspCore.DLL.DbRepository
{
    public class BrandRepository : IRepository<Brand>
    {
        private ShopContext context;

        public BrandRepository(ShopContext context)
        {
            this.context = context;
        }
        public async Task Create(Brand item)
        {
            await context.Brands.AddAsync(item);
        }

        public async Task Delete(int Id)
        {
            var brand = context.Brands.FirstOrDefault(xx => xx.Id == Id);
            await Task.Run(() => context.Brands.Remove(brand));
        }

        public async Task<IQueryable<Brand>> Find(Expression<Func<Brand, bool>> predicate)
        {
            var br = await Task.Run(() => context.Brands.Include(xx=>xx.CategoryBrands).ThenInclude(cb=>cb.Category).ThenInclude(g=>g.Goods).Where(predicate));
            return br.AsQueryable();
        }

        public async Task<Brand> Get(int Id)
        {
            return await Task.Run(() => context.Brands.FirstOrDefault(xx => xx.Id == Id));
        }

        public Task<Brand> Get(string Id)
        {
            throw new NotImplementedException();
        }

        public async Task<IQueryable<Brand>> GetAll()
        {
            var br = await Task.Run(() => context.Brands.Include(xx=>xx.CategoryBrands).AsQueryable());
            return br;
        }        

        public async Task Update(Brand item)
        {
            await Task.Run(() => context.Entry(item).State = EntityState.Modified);
        }
    }
}
