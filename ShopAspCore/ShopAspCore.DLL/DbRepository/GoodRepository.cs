﻿using ShopAspCore.DLL.Entities;
using ShopAspCore.DLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using ShopAspCore.DLL.Context;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Linq.Expressions;

namespace ShopAspCore.DLL.DbRepository
{
    public class GoodRepository : IRepository<Good>
    {
        private ShopContext context;

        public GoodRepository(ShopContext context)
        {
            this.context = context;
        }
        public async Task Create(Good item)
        {
            await context.Goods.AddAsync(item);
        }

        public async Task Delete(int Id)
        {
            var item = context.Goods.FirstOrDefault(xx => xx.Id == Id);
            await Task.Run(() => context.Goods.Remove(item));
        }

        public async Task<IQueryable<Good>> Find(Expression<Func<Good, bool>> predicate)
        {
            return await Task.Run(() => context.Goods.Include(bb=>bb.Basket).Include(u=>u.User.ShopUser).Include(bb=>bb.Brand).Include(xx=>xx.Category).Where(predicate).AsQueryable());
        }

        public async Task<Good> Get(int Id)
        {
            return await Task.Run(() => context.Goods.Include(br=>br.Brand).Include(ca=>ca.Category).Include(u=>u.User.ShopUser).Include(ba=>ba.Basket).FirstOrDefault(xx => xx.Id == Id));
        }

        public Task<Good> Get(string Id)
        {
            throw new NotImplementedException();
        }

        public async Task<IQueryable<Good>> GetAll()
        {
            return await Task.Run(() => context.Goods.Include(xx=>xx.Brand).Include(cc=>cc.Category).Include(zz=>zz.User).AsQueryable());
        }
        
        public async Task Update(Good item)
        {
            var good =await Get(item.Id);
            //good.Basket = item.Basket;
            good.BasketId = item.BasketId;
            good.SoldOut = item.SoldOut;
            good.DateOfSale = item.DateOfSale;
            await Task.Run(() => context.Entry(good).State = EntityState.Modified);
        }
    }
}
