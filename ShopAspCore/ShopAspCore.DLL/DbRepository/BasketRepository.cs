﻿using ShopAspCore.DLL.Entities;
using ShopAspCore.DLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using ShopAspCore.DLL.Context;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Linq.Expressions;

namespace ShopAspCore.DLL.DbRepository
{
    public class BasketRepository : IRepository<Basket>
    {
        private ShopContext context;

        public BasketRepository(ShopContext context)
        {
            this.context = context;
        }
        public async Task Create(Basket item)
        {
            await context.Baskets.AddAsync(item);
        }

        public async Task Delete(int Id)
        {
            var bask = context.Baskets.FirstOrDefault(xx => xx.Id == Id);
            await Task.Run(()=> context.Remove(bask));            
        }        

        public async Task<IQueryable<Basket>> Find(Expression<Func<Basket, bool>> predicate)
        {
            return await Task.Run(() => context.Baskets.Where(predicate).AsQueryable());
        }

        public async Task<Basket> Get(int Id)
        {
            return await Task.Run(() => context.Baskets.Include(xx=>xx.UserProfile).FirstOrDefault(xx=>xx.Id == Id));
        }

        public Task<Basket> Get(string Id)
        {
            throw new NotImplementedException();
        }

        public async Task<IQueryable<Basket>> GetAll()
        {
            return await Task.Run(() => context.Baskets.Include(xx=>xx.Goods).Include(xx=>xx.UserProfile).AsQueryable());
        }        

        public async Task Update(Basket item)
        {
            await Task.Run(() => context.Entry(item).State = EntityState.Modified);
        }
    }
}
