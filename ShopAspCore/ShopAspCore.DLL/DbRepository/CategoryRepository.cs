﻿using ShopAspCore.DLL.Entities;
using ShopAspCore.DLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using ShopAspCore.DLL.Context;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Linq.Expressions;

namespace ShopAspCore.DLL.DbRepository
{
    class CategoryRepository : IRepository<Category>
    {
        private ShopContext context;

        public CategoryRepository(ShopContext context)
        {
            this.context = context;
        }
        public async Task Create(Category item)
        {
            await context.Categories.AddAsync(item);
        }

        public async Task Delete(int Id)
        {
            var categ = context.Categories.FirstOrDefault(xx => xx.Id == Id);
            await Task.Run(() => context.Categories.Remove(categ));
        }

        public async Task<IQueryable<Category>> Find(Expression<Func<Category, bool>> predicate)
        {
            return await Task.Run(() => context.Categories.Where(predicate).AsQueryable());
        }

        public async Task<Category> Get(int Id)
        {
            return await Task.Run(() => context.Categories.FirstOrDefault(xx => xx.Id == Id));
        }

        public Task<Category> Get(string Id)
        {
            throw new NotImplementedException();
        }

        public async Task<IQueryable<Category>> GetAll()
        {
            var fff = await Task.Run(() => context.Categories.Include(xx => xx.CategoryBrands).ThenInclude(bb=>bb.Brand).ThenInclude(gg=>gg.Goods));
            return fff;
        }
        

        public async Task Update(Category item)
        {
            await Task.Run(() => context.Entry(item).State = EntityState.Modified);
        }

        
    }
}
