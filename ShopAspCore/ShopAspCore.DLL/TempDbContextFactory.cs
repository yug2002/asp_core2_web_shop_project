﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using ShopAspCore.DLL.Context;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace ShopAspCore.DLL
{
    public class TempDbContextFactory : IDesignTimeDbContextFactory<ShopContext>
    {
        public ShopContext CreateDbContext(string[] args)
        {
            var configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
            .Build();

            var builder = new DbContextOptionsBuilder<ShopContext>();
            var connectionString = configuration.GetConnectionString("DefaultConnection");
            builder.UseSqlServer(connectionString,
                sqlServerOptionsAction: optionsBuilder => optionsBuilder.MigrationsAssembly(typeof(ShopContext).GetTypeInfo().Assembly.GetName().Name));
            return new ShopContext(builder.Options);
        }
    }
}
