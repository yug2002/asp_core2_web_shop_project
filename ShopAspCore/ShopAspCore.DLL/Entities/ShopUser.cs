﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShopAspCore.DLL.Entities
{
    public class ShopUser: IdentityUser
    {
        public UserProfile UserProfile { get; set; }
        
    }
}
