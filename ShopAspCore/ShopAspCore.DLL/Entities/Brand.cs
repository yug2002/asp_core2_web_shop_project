﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShopAspCore.DLL.Entities
{    
    public class Brand
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual List<Good> Goods { get; set; }

        public List<CategoryBrand> CategoryBrands { get; } = new List<CategoryBrand>();
    }
}
