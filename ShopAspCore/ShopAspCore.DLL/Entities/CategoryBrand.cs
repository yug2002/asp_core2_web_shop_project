﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShopAspCore.DLL.Entities
{
    public class CategoryBrand
    {
        public int BrandId { get; set; }
        public Brand Brand { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }
    }
}
