﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using ShopAspCore.DLL.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ShopAspCore.DLL.Entities.OverrideIdentity
{
    public class ShopUserStore : UserStore<ShopUser, IdentityRole, ShopContext>
    {
        public ShopUserStore(ShopContext context, IdentityErrorDescriber describer = null) : base(context, describer)
        {
        }

        public override IQueryable<ShopUser> Users => base.Users;

        public override Task AddClaimsAsync(ShopUser user, IEnumerable<Claim> claims, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.AddClaimsAsync(user, claims, cancellationToken);
        }

        public override Task AddLoginAsync(ShopUser user, UserLoginInfo login, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.AddLoginAsync(user, login, cancellationToken);
        }

        public override Task AddToRoleAsync(ShopUser user, string normalizedRoleName, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.AddToRoleAsync(user, normalizedRoleName, cancellationToken);
        }

        public override string ConvertIdFromString(string id)
        {
            return base.ConvertIdFromString(id);
        }

        public override string ConvertIdToString(string id)
        {
            return base.ConvertIdToString(id);
        }

        public override Task<int> CountCodesAsync(ShopUser user, CancellationToken cancellationToken)
        {
            return base.CountCodesAsync(user, cancellationToken);
        }

        public override Task<IdentityResult> CreateAsync(ShopUser user, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.CreateAsync(user, cancellationToken);
        }

        public override Task<IdentityResult> DeleteAsync(ShopUser user, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.DeleteAsync(user, cancellationToken);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override Task<ShopUser> FindByEmailAsync(string normalizedEmail, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.FindByEmailAsync(normalizedEmail, cancellationToken);
        }

        public override Task<ShopUser> FindByIdAsync(string userId, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.FindByIdAsync(userId, cancellationToken);
        }

        public override Task<ShopUser> FindByLoginAsync(string loginProvider, string providerKey, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.FindByLoginAsync(loginProvider, providerKey, cancellationToken);
        }

        public override Task<ShopUser> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.FindByNameAsync(normalizedUserName, cancellationToken);
        }

        public override Task<int> GetAccessFailedCountAsync(ShopUser user, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.GetAccessFailedCountAsync(user, cancellationToken);
        }

        public override Task<string> GetAuthenticatorKeyAsync(ShopUser user, CancellationToken cancellationToken)
        {
            return base.GetAuthenticatorKeyAsync(user, cancellationToken);
        }

        public override Task<IList<Claim>> GetClaimsAsync(ShopUser user, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.GetClaimsAsync(user, cancellationToken);
        }

        public override Task<string> GetEmailAsync(ShopUser user, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.GetEmailAsync(user, cancellationToken);
        }

        public override Task<bool> GetEmailConfirmedAsync(ShopUser user, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.GetEmailConfirmedAsync(user, cancellationToken);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override Task<bool> GetLockoutEnabledAsync(ShopUser user, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.GetLockoutEnabledAsync(user, cancellationToken);
        }

        public override Task<DateTimeOffset?> GetLockoutEndDateAsync(ShopUser user, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.GetLockoutEndDateAsync(user, cancellationToken);
        }

        public override Task<IList<UserLoginInfo>> GetLoginsAsync(ShopUser user, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.GetLoginsAsync(user, cancellationToken);
        }

        public override Task<string> GetNormalizedEmailAsync(ShopUser user, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.GetNormalizedEmailAsync(user, cancellationToken);
        }

        public override Task<string> GetNormalizedUserNameAsync(ShopUser user, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.GetNormalizedUserNameAsync(user, cancellationToken);
        }

        public override Task<string> GetPasswordHashAsync(ShopUser user, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.GetPasswordHashAsync(user, cancellationToken);
        }

        public override Task<string> GetPhoneNumberAsync(ShopUser user, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.GetPhoneNumberAsync(user, cancellationToken);
        }

        public override Task<bool> GetPhoneNumberConfirmedAsync(ShopUser user, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.GetPhoneNumberConfirmedAsync(user, cancellationToken);
        }

        public override Task<IList<string>> GetRolesAsync(ShopUser user, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.GetRolesAsync(user, cancellationToken);
        }

        public override Task<string> GetSecurityStampAsync(ShopUser user, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.GetSecurityStampAsync(user, cancellationToken);
        }

        public override Task<string> GetTokenAsync(ShopUser user, string loginProvider, string name, CancellationToken cancellationToken)
        {
            return base.GetTokenAsync(user, loginProvider, name, cancellationToken);
        }

        public override Task<bool> GetTwoFactorEnabledAsync(ShopUser user, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.GetTwoFactorEnabledAsync(user, cancellationToken);
        }

        public override Task<string> GetUserIdAsync(ShopUser user, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.GetUserIdAsync(user, cancellationToken);
        }

        public override Task<string> GetUserNameAsync(ShopUser user, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.GetUserNameAsync(user, cancellationToken);
        }

        public override Task<IList<ShopUser>> GetUsersForClaimAsync(Claim claim, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.GetUsersForClaimAsync(claim, cancellationToken);
        }

        public override Task<IList<ShopUser>> GetUsersInRoleAsync(string normalizedRoleName, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.GetUsersInRoleAsync(normalizedRoleName, cancellationToken);
        }

        public override Task<bool> HasPasswordAsync(ShopUser user, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.HasPasswordAsync(user, cancellationToken);
        }

        public override Task<int> IncrementAccessFailedCountAsync(ShopUser user, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.IncrementAccessFailedCountAsync(user, cancellationToken);
        }

        public override Task<bool> IsInRoleAsync(ShopUser user, string normalizedRoleName, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.IsInRoleAsync(user, normalizedRoleName, cancellationToken);
        }

        public override Task<bool> RedeemCodeAsync(ShopUser user, string code, CancellationToken cancellationToken)
        {
            return base.RedeemCodeAsync(user, code, cancellationToken);
        }

        public override Task RemoveClaimsAsync(ShopUser user, IEnumerable<Claim> claims, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.RemoveClaimsAsync(user, claims, cancellationToken);
        }

        public override Task RemoveFromRoleAsync(ShopUser user, string normalizedRoleName, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.RemoveFromRoleAsync(user, normalizedRoleName, cancellationToken);
        }

        public override Task RemoveLoginAsync(ShopUser user, string loginProvider, string providerKey, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.RemoveLoginAsync(user, loginProvider, providerKey, cancellationToken);
        }

        public override Task RemoveTokenAsync(ShopUser user, string loginProvider, string name, CancellationToken cancellationToken)
        {
            return base.RemoveTokenAsync(user, loginProvider, name, cancellationToken);
        }

        public override Task ReplaceClaimAsync(ShopUser user, Claim claim, Claim newClaim, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.ReplaceClaimAsync(user, claim, newClaim, cancellationToken);
        }

        public override Task ReplaceCodesAsync(ShopUser user, IEnumerable<string> recoveryCodes, CancellationToken cancellationToken)
        {
            return base.ReplaceCodesAsync(user, recoveryCodes, cancellationToken);
        }

        public override Task ResetAccessFailedCountAsync(ShopUser user, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.ResetAccessFailedCountAsync(user, cancellationToken);
        }

        public override Task SetAuthenticatorKeyAsync(ShopUser user, string key, CancellationToken cancellationToken)
        {
            return base.SetAuthenticatorKeyAsync(user, key, cancellationToken);
        }

        public override Task SetEmailAsync(ShopUser user, string email, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.SetEmailAsync(user, email, cancellationToken);
        }

        public override Task SetEmailConfirmedAsync(ShopUser user, bool confirmed, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.SetEmailConfirmedAsync(user, confirmed, cancellationToken);
        }

        public override Task SetLockoutEnabledAsync(ShopUser user, bool enabled, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.SetLockoutEnabledAsync(user, enabled, cancellationToken);
        }

        public override Task SetLockoutEndDateAsync(ShopUser user, DateTimeOffset? lockoutEnd, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.SetLockoutEndDateAsync(user, lockoutEnd, cancellationToken);
        }

        public override Task SetNormalizedEmailAsync(ShopUser user, string normalizedEmail, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.SetNormalizedEmailAsync(user, normalizedEmail, cancellationToken);
        }

        public override Task SetNormalizedUserNameAsync(ShopUser user, string normalizedName, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.SetNormalizedUserNameAsync(user, normalizedName, cancellationToken);
        }

        public override Task SetPasswordHashAsync(ShopUser user, string passwordHash, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.SetPasswordHashAsync(user, passwordHash, cancellationToken);
        }

        public override Task SetPhoneNumberAsync(ShopUser user, string phoneNumber, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.SetPhoneNumberAsync(user, phoneNumber, cancellationToken);
        }

        public override Task SetPhoneNumberConfirmedAsync(ShopUser user, bool confirmed, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.SetPhoneNumberConfirmedAsync(user, confirmed, cancellationToken);
        }

        public override Task SetSecurityStampAsync(ShopUser user, string stamp, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.SetSecurityStampAsync(user, stamp, cancellationToken);
        }

        public override Task SetTokenAsync(ShopUser user, string loginProvider, string name, string value, CancellationToken cancellationToken)
        {
            return base.SetTokenAsync(user, loginProvider, name, value, cancellationToken);
        }

        public override Task SetTwoFactorEnabledAsync(ShopUser user, bool enabled, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.SetTwoFactorEnabledAsync(user, enabled, cancellationToken);
        }

        public override Task SetUserNameAsync(ShopUser user, string userName, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.SetUserNameAsync(user, userName, cancellationToken);
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override Task<IdentityResult> UpdateAsync(ShopUser user, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.UpdateAsync(user, cancellationToken);
        }

        protected override Task AddUserTokenAsync(IdentityUserToken<string> token)
        {
            return base.AddUserTokenAsync(token);
        }

        protected override IdentityUserClaim<string> CreateUserClaim(ShopUser user, Claim claim)
        {
            return base.CreateUserClaim(user, claim);
        }

        protected override IdentityUserLogin<string> CreateUserLogin(ShopUser user, UserLoginInfo login)
        {
            return base.CreateUserLogin(user, login);
        }

        protected override IdentityUserRole<string> CreateUserRole(ShopUser user, IdentityRole role)
        {
            return base.CreateUserRole(user, role);
        }

        protected override IdentityUserToken<string> CreateUserToken(ShopUser user, string loginProvider, string name, string value)
        {
            return base.CreateUserToken(user, loginProvider, name, value);
        }

        protected override Task<IdentityRole> FindRoleAsync(string normalizedRoleName, CancellationToken cancellationToken)
        {
            return base.FindRoleAsync(normalizedRoleName, cancellationToken);
        }

        protected override Task<IdentityUserToken<string>> FindTokenAsync(ShopUser user, string loginProvider, string name, CancellationToken cancellationToken)
        {
            return base.FindTokenAsync(user, loginProvider, name, cancellationToken);
        }

        protected override Task<ShopUser> FindUserAsync(string userId, CancellationToken cancellationToken)
        {
            return base.FindUserAsync(userId, cancellationToken);
        }

        protected override Task<IdentityUserLogin<string>> FindUserLoginAsync(string userId, string loginProvider, string providerKey, CancellationToken cancellationToken)
        {
            return base.FindUserLoginAsync(userId, loginProvider, providerKey, cancellationToken);
        }

        protected override Task<IdentityUserLogin<string>> FindUserLoginAsync(string loginProvider, string providerKey, CancellationToken cancellationToken)
        {
            return base.FindUserLoginAsync(loginProvider, providerKey, cancellationToken);
        }

        protected override Task<IdentityUserRole<string>> FindUserRoleAsync(string userId, string roleId, CancellationToken cancellationToken)
        {
            return base.FindUserRoleAsync(userId, roleId, cancellationToken);
        }

        protected override Task RemoveUserTokenAsync(IdentityUserToken<string> token)
        {
            return base.RemoveUserTokenAsync(token);
        }
    }
}
