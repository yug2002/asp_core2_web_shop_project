﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShopAspCore.DLL.Entities
{
    public class Basket
    {
        public int Id { get; set; }

        public virtual ICollection<Good> Goods { get; set; }        

        public string UserProfileForeignKey { get; set; }
        public UserProfile UserProfile { get; set; }

    }
}
