﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShopAspCore.DLL.Entities
{    
    public class UserProfile
    {        
        public string Id { get; set; }
        public string LastName { get; set; }
        public string Firstname { get; set; }
        public string Address { get; set; }

        public string ShopUserForeignKey { get; set; }
        public virtual ShopUser ShopUser{ get; set;}
        
        public Basket Basket { get; set; }       

        public virtual ICollection<Good> Goods { get; set; }
    }
}
