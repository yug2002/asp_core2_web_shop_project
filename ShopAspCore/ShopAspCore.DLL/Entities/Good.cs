﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShopAspCore.DLL.Entities
{    
    public class Good
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public DateTime ReceiptDate { get; set; }
        public DateTime? DateOfSale { get; set; }

        public int CategoryId { get; set; }        
        public Category Category { get; set; }

        public string Code { get; set; }

        public int BrandId { get; set; }        
        public Brand Brand { get; set; }

        public string Description { get; set; }
        public string Image { get; set; }
        public string ImageB { get; set; }

        public string UserId { get; set; }        
        public UserProfile User { get; set; }  

        public int? BasketId { get; set; }
        public Basket Basket { get; set; }

        public bool SoldOut { get; set; }

        public bool IsExist { get; set; }
    }
}

