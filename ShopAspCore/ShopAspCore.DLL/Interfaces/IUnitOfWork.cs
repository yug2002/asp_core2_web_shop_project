﻿using Microsoft.AspNetCore.Identity;
using ShopAspCore.DLL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ShopAspCore.DLL.Interfaces
{
    public interface IUnitOfWork:IDisposable
    {
        IRepository<Good> Goods { get; }        
        IRepository<Category> Categories { get; }
        IRepository<Basket> Baskets { get; }        
        IRepository<Brand> Brands { get; }
        IRepository<UserProfile> UserProfiles { get; }
        IEnumerable<IdentityRole> Roles { get; }
        Task<int> SaveAsync();
    }
}
