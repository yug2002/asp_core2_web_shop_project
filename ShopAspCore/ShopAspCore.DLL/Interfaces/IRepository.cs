﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ShopAspCore.DLL.Interfaces
{
    public interface IRepository<T> where T: class
    {
        Task<IQueryable<T>> GetAll();        
        Task<T> Get(int Id);
        Task<T> Get(string Id);
        Task<IQueryable<T>> Find(Expression<Func<T,bool>> predicate);
        Task Create(T item);
        Task Update(T item);
        Task Delete(int Id);

    }
}
