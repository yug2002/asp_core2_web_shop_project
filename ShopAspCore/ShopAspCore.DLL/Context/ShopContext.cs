﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ShopAspCore.DLL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShopAspCore.DLL.Context
{
    public class ShopContext:IdentityDbContext<ShopUser>
    {
        static ShopContext()
        {

        }
        public ShopContext(DbContextOptions<ShopContext> options) : base(options) { }

        public DbSet<Basket> Baskets { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<Category> Categories { get; set; }       
        public DbSet<Good> Goods { get; set; }        
        public DbSet<UserProfile> UserProfiles { get; set; }
        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<CategoryBrand>()
                .HasKey(t => new { t.CategoryId, t.BrandId });

            modelBuilder.Entity<CategoryBrand>()
                .HasOne(c => c.Category)
                .WithMany(cb => cb.CategoryBrands)
                .HasForeignKey(c => c.CategoryId);

            modelBuilder.Entity<CategoryBrand>()
                .HasOne(b => b.Brand)
                .WithMany(cb => cb.CategoryBrands)
                .HasForeignKey(b => b.BrandId);

            modelBuilder.Entity<UserProfile>()
                .HasOne(z => z.Basket)
                .WithOne(x => x.UserProfile)
                .HasForeignKey<Basket>(b => b.UserProfileForeignKey);            

            modelBuilder.Entity<Basket>()
                .HasMany(g => g.Goods)
                .WithOne(b => b.Basket)
                .HasForeignKey(k => k.BasketId);

            modelBuilder.Entity<ShopUser>()
                .HasOne(z => z.UserProfile)
                .WithOne(x => x.ShopUser)
                .HasForeignKey<UserProfile>(b => b.ShopUserForeignKey);
           
        }
    }
}
