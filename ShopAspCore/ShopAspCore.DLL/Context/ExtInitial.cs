﻿using Microsoft.AspNetCore.Identity;
using ShopAspCore.DLL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShopAspCore.DLL.Context
{
    public static class ExtInitial
    {
        public static void EnsureSeedData(this ShopContext context)
        {
            if (!context.Categories.Any())
            {
                context.Roles.AddRange(
                    new IdentityRole { Name = "Admin", NormalizedName = "ADMIN" },
                    new IdentityRole { Name = "User", NormalizedName = "USER" });
                context.SaveChanges();

                var uAdminRole = context.Roles.Where(xx => xx.Name == "Admin").FirstOrDefault();
                var uUserRole = context.Roles.Where(xx => xx.Name == "User").FirstOrDefault();
                
                context.AddRange(CreateUserProfile());
                context.SaveChanges();

                var arr = context.UserProfiles.Select(xx => xx.Id).ToList();                
                var idAdmin = (context.UserProfiles.Where(xx => xx.ShopUser.UserName == "admin").FirstOrDefault()).ShopUser.Id;
                var idUsers = context.UserProfiles.Where(xx => xx.ShopUser.UserName != "admin").Select(id=>id.ShopUser.Id).ToList();

                context.UserRoles.AddRange(
                    new IdentityUserRole<string> { RoleId = uAdminRole.Id, UserId = idAdmin },
                    new IdentityUserRole<string> { RoleId = uUserRole.Id, UserId = idUsers[0] },
                    new IdentityUserRole<string> { RoleId = uUserRole.Id, UserId = idUsers[1] },
                    new IdentityUserRole<string> { RoleId = uUserRole.Id, UserId = idUsers[2] },
                    new IdentityUserRole<string> { RoleId = uUserRole.Id, UserId = idUsers[3] });
                context.SaveChanges();


                context.AddRange(CreateCategory());
                context.SaveChanges();

                context.AddRange(CreateBrand());
                context.SaveChanges();

                context.AddRange(CreateCategoryBrand());
                context.SaveChanges();

                context.AddRange(CreateGoods(arr));
                context.SaveChanges();

                //context.AddRange(CreateBaskets());
                //context.SaveChanges();

            }
        }
        #region Helpers
        private static List<Category> CreateCategory()
        {
            List<Category> categories = new List<Category>()
            {
                new Category { Name = "Телевизоры" }, new Category { Name = "Холодильники" },
                new Category { Name = "Стиральные машины" }, new Category { Name = "Газовые плиты"},
                new Category { Name = "Кондиционеры"}
            };
            return categories;
        }

        private static List<CategoryBrand> CreateCategoryBrand()
        {
            List<CategoryBrand> categoryBrands = new List<CategoryBrand>()
            {
                //телевизоры
                new CategoryBrand { CategoryId = 1, BrandId = 1 },
                new CategoryBrand { CategoryId = 1, BrandId = 2 },
                new CategoryBrand { CategoryId = 1, BrandId = 3 },
                new CategoryBrand { CategoryId = 1, BrandId = 5 },
                new CategoryBrand { CategoryId = 1, BrandId = 7 },
                new CategoryBrand { CategoryId = 1, BrandId = 9 },
                //холодильники
                new CategoryBrand { CategoryId = 2, BrandId = 4 },
                new CategoryBrand { CategoryId = 2, BrandId = 5 },
                new CategoryBrand { CategoryId = 2, BrandId = 7 },
                new CategoryBrand { CategoryId = 2, BrandId = 8 },
                //стиральные машины
                new CategoryBrand { CategoryId = 3, BrandId = 4 },
                new CategoryBrand { CategoryId = 3, BrandId = 5 },
                new CategoryBrand { CategoryId = 3, BrandId = 7 },
                new CategoryBrand { CategoryId = 3, BrandId = 8 },
                //газовые плиты                
                new CategoryBrand { CategoryId = 4, BrandId = 6 },
                new CategoryBrand { CategoryId = 4, BrandId = 8 },
                new CategoryBrand { CategoryId = 4, BrandId = 12 },
                new CategoryBrand { CategoryId = 4, BrandId = 13 },
                //кондиционеры
                new CategoryBrand { CategoryId = 5, BrandId = 9 },
                new CategoryBrand { CategoryId = 5, BrandId = 10 },
                new CategoryBrand { CategoryId = 5, BrandId = 11 }
                
            };
            return categoryBrands;
        }

        private static List<Brand> CreateBrand()
        {
            List<Brand> brands = new List<Brand>()
            {
                new Brand { Name = "Horizont"},
                new Brand { Name = "SONY"},
                new Brand { Name = "Philips"},
                new Brand { Name = "Atlant"},
                new Brand { Name = "Samsung"},
                new Brand { Name = "Gefest"},
                new Brand { Name = "LG"},
                new Brand { Name = "Indesit"},
                new Brand { Name = "Panasonic"},
                new Brand { Name = "Daikin"},
                new Brand { Name = "Komatsu"},
                new Brand { Name = "Cezaris"},
                new Brand { Name = "Hansa"}
            };
            return brands;
        }

        private static List<Good> CreateGoods(List<string> list)
        {
            List<Good> goods = new List<Good>()
            {
                new Good
                {
                    Name = "24LE5181D",
                    BrandId = 1,
                    CategoryId = 1,
                    ReceiptDate = new DateTime(2017, 11, 05),
                    Description = "LCD-телевизор c диагональю экрана 24 дюйма, LED-подсветкой и встроенным"+
                    " цифровым тюнером (DVB-Т2/T, DVB-C)",
                    UserId = list.ElementAt(0),
                    Code = ToGuid(new DateTime(2017, 11, 05)).ToString(),
                    Price = 400,
                    Image = "\\images\\TV\\Horizont\\big\\24LE5181D.JPG",
                    ImageB = "\\images\\TV\\Horizont\\small\\24LE5181Ds.jpg",
                    IsExist = true,
                    SoldOut = false
                
                    

                },
                new Good
                {
                    Name = "32LE5161D",
                    BrandId = 1,
                    CategoryId = 1,
                    ReceiptDate = new DateTime(2017, 11, 12),
                    Description = "32\" 1366x768 (HD), матрица IPS",
                    UserId = list.ElementAt(0),
                    Code = ToGuid(new DateTime(2017, 11, 12)).ToString(),
                    Price = 520,
                    Image = "\\images\\TV\\Horizont\\big\\32LE5161D - 1.jpg",
                    ImageB = "\\images\\TV\\Horizont\\small\\32LE5161D---1s.jpg",
                    IsExist = true,
                    SoldOut = false

                },
                new Good
                {
                    Name = "43LE5173D",
                    BrandId = 1,
                    CategoryId = 1,
                    ReceiptDate = new DateTime(2017, 11, 18),
                    Description = "ЖК-телевизор, 1080p Full HD диагональ 43\" (109 см) HDMI x3, USB x2, DVB-T2",
                    UserId = list.ElementAt(1),
                    Code = ToGuid(new DateTime(2017, 11, 18)).ToString(),
                    Price = 730,
                    Image = "\\images\\TV\\Horizont\\big\\43LE5173D.jpg",
                    ImageB = "\\images\\TV\\Horizont\\small\\43LE5173Ds.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "49LE5188D",
                    BrandId = 1,
                    CategoryId = 1,
                    ReceiptDate = new DateTime(2017, 11, 21),
                    Description = "ЖК-телевизор, 1080p Full HD диагональ 49\" (123 см) HDMI x3, USB x2, DVB-T2",
                    UserId = list.ElementAt(1),
                    Code = ToGuid(new DateTime(2017, 11, 21)).ToString(),
                    Price = 999,
                    Image = "\\images\\TV\\Horizont\\big\\49.jpg",
                    ImageB = "\\images\\TV\\Horizont\\small\\49s.jpg",
                    IsExist = true,
                    SoldOut = false

                },
                new Good
                {
                    Name = "XE90",
                    BrandId = 2,
                    CategoryId = 1,
                    ReceiptDate = new DateTime(2017, 12, 01),
                    Description = "Реалистичное изображение с X-tended Dynamic Range™ PRO"+
                    "4K HDR Processor X1™: Больше деталей. Больше нюансов. Больше жизни"+
                    "Дисплей TRILUMINOS™: выше яркость, больше цветов",
                    UserId = list.ElementAt(1),
                    Code = ToGuid(new DateTime(2017, 12, 01)).ToString(),
                    Price = 2500,
                    Image = "\\images\\TV\\Sony\\big\\sonyXE90.jpg",
                    ImageB = "\\images\\TV\\Sony\\small\\sonyXE90s.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "XE94",
                    BrandId = 2,
                    CategoryId = 1,
                    ReceiptDate = new DateTime(2017, 12, 02),
                    Description = "Этот телевизор сочетает яркость, богатство оттенков и "+
                    "расширенный динамический диапазон с ошеломительной четкостью 4K, а также"+"" +
                    " поддерживает различные форматы HDR, включая HDR10, Hybrid Log-Gamma и "+
                    "Dolby Vision™3.",
                    UserId = list.ElementAt(1),
                    Code = ToGuid(new DateTime(2017, 12, 02)).ToString(),
                    Price = 2880,
                    Image = "\\images\\TV\\Sony\\big\\sonyXE94.jpg",
                    ImageB = "\\images\\TV\\Sony\\small\\sonyXE94s.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "ZD9",
                    BrandId = 2,
                    CategoryId = 1,
                    ReceiptDate = new DateTime(2017, 12, 03),
                    Description = "В Sony KD-65ZD9 применены в общей сложности три новых технологии:"+
                    " объектно-ориентированный HDR ремастеринг, двойная обработка базовых данных и"+
                    " плавность цветовых переходов Super Bit Mapping 4K HDR. Эти новшества при"+
                    " воспроизведении 4K HDR контента выводят модели на совершенно новый уровень",
                    UserId = list.ElementAt(1),
                    Code = ToGuid(new DateTime(2017, 12, 03)).ToString(),
                    Price = 6000,
                    Image = "\\images\\TV\\Sony\\big\\sonyZD9.jpg",
                    ImageB = "\\images\\TV\\Sony\\small\\sonyZD9s.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "32LJ600U",
                    BrandId = 7,
                    CategoryId = 1,
                    ReceiptDate = new DateTime(2017, 12, 14),
                    Description = "32д 1366x768 (HD), частота матрицы 50 Гц, индекс динамичных сцен 900, Smart TV (LG webOS), Wi-Fi",
                    UserId = list.ElementAt(0),
                    Code = ToGuid(new DateTime(2017, 12, 14)).ToString(),
                    Price = 500,
                    Image = "\\images\\TV\\LG\\big\\32LJ600U.jpg",
                    ImageB = "\\images\\TV\\LG\\small\\32LJ600Us.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "43UJ634V",
                    BrandId = 7,
                    CategoryId = 1,
                    ReceiptDate = new DateTime(2017, 12, 15),
                    Description = "Высококачественная IPS панель Активный HDR Ultra Surround webOS 3.5",
                    UserId = list.ElementAt(0),
                    Code = ToGuid(new DateTime(2017, 12, 15)).ToString(),
                    Price = 1400,
                    Image = "\\images\\TV\\LG\\big\\43UJ634V.jpg",
                    ImageB = "\\images\\TV\\LG\\small\\43UJ634Vs.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "24PHT4032",
                    BrandId = 3,
                    CategoryId = 1,
                    ReceiptDate = new DateTime(2017, 12, 16),
                    Description = "Высококачественная IPS панель Активный HDR Ultra Surround webOS 3.5",
                    UserId = list.ElementAt(2),
                    Code = ToGuid(new DateTime(2017, 12, 16)).ToString(),
                    Price = 850,
                    Image = "\\images\\TV\\Philips\\big\\24PHT4032.jpg",
                    ImageB = "\\images\\TV\\Philips\\small\\24PHT4032s.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "55PUT6162",
                    BrandId = 3,
                    CategoryId = 1,
                    ReceiptDate = new DateTime(2017, 12, 17),
                    Description = "6000 series Ультратонкий светодиодный телевизор 4K Smart LED TV 139 см (55\"),"+
                    " 4K Ultra HD LED TV, Quad Core, DVB-T/T2/C  55PUT6162/12",
                    UserId = list.ElementAt(2),
                    Code = ToGuid(new DateTime(2017, 12, 17)).ToString(),
                    Price = 1850,
                    Image = "\\images\\TV\\Philips\\big\\55PUT6162.jpg",
                    ImageB = "\\images\\TV\\Philips\\small\\55PUT6162s.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "UE43M5500AU",
                    BrandId = 5,
                    CategoryId = 1,
                    ReceiptDate = new DateTime(2017, 12, 18),
                    Description = "43\" 1920x1080 (Full HD), частота матрицы 50 Гц, индекс"+
                    " динамичных сцен 800, Smart TV (Samsung Tizen), Wi-Fi",
                    UserId = list.ElementAt(3),
                    Code = ToGuid(new DateTime(2017, 12, 18)).ToString(),
                    Price = 1150,
                    Image = "\\images\\TV\\Samsung\\big\\UE43M5500AU.jpg",
                    ImageB = "\\images\\TV\\Samsung\\small\\UE43M5500AUs.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "UE49MU6300U",
                    BrandId = 5,
                    CategoryId = 1,
                    ReceiptDate = new DateTime(2017, 12, 19),
                    Description = "49\" 3840x2160 (4K UHD), частота матрицы 50 Гц, индекс динамичных"+
                    " сцен 1400, Smart TV (Samsung Tizen), HDR, Wi-Fi",
                    UserId = list.ElementAt(3),
                    Code = ToGuid(new DateTime(2017, 12, 19)).ToString(),
                    Price = 1880,
                    Image = "\\images\\TV\\Samsung\\big\\UE49MU6300U.jpg",
                    ImageB = "\\images\\TV\\Samsung\\small\\UE49MU6300Us.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "TX-32ES400E",
                    BrandId = 9,
                    CategoryId = 1,
                    ReceiptDate = new DateTime(2017, 12, 19),
                    Description = "32\" 1366x768 (HD), частота матрицы 50 Гц, Smart TV (Panasonic my Home Screen), Wi-Fi",
                    UserId = list.ElementAt(1),
                    Code = ToGuid(new DateTime(2017, 12, 19)).ToString(),
                    Price = 500,
                    Image = "\\images\\TV\\panasonic\\big\\TX-32ES400E.jpeg",
                    ImageB = "\\images\\TV\\panasonic\\small\\TX-32ES400E.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "TX-40EXR600",
                    BrandId = 9,
                    CategoryId = 1,
                    ReceiptDate = new DateTime(2017, 12, 03),
                    Description = "40\" 3840x2160 (4K UHD), матрица VA, частота матрицы 50 Гц, индекс динамичных сцен 1300, Smart TV (Panasonic my Home Screen 2.0 (Firefox)), HDR, Wi-Fi",
                    UserId = list.ElementAt(1),
                    Code = ToGuid(new DateTime(2017, 12, 03)).ToString(),
                    Price = 1100,
                    Image = "\\images\\TV\\panasonic\\big\\TX-40EXR600.jpeg",
                    ImageB = "\\images\\TV\\panasonic\\small\\TX-40EXR600.jpg",
                    IsExist = true,
                    SoldOut = false
                },

                new Good
                {
                    Name = "ХМ 4425-069 ND",
                    BrandId = 4,
                    CategoryId = 2,
                    ReceiptDate = new DateTime(2017, 12, 08),
                    Description = "Холодильник с морозильником ATLANT ХМ 4425-069 ND",
                    UserId = list.ElementAt(0),
                    Code = ToGuid(new DateTime(2017, 12, 08)).ToString(),
                    Price = 923,
                    Image = "\\images\\fridges\\atlant\\big\\ХМ 4425-069 ND.jpg",
                    ImageB = "\\images\\fridges\\atlant\\small\\ХМ 4425-069 NDs.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "ХМ 4624-101",
                    BrandId = 4,
                    CategoryId = 2,
                    ReceiptDate = new DateTime(2017, 12, 09),
                    Description = "Холодильник с морозильником ATLANT ХМ 4624-101",
                    UserId = list.ElementAt(0),
                    Code = ToGuid(new DateTime(2017, 12, 09)).ToString(),
                    Price = 710,
                    Image = "\\images\\fridges\\atlant\\big\\ХМ 4624-101.jpg",
                    ImageB = "\\images\\fridges\\atlant\\small\\ХМ 4624-101s.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "DF 5180 E",
                    BrandId = 8,
                    CategoryId = 2,
                    ReceiptDate = new DateTime(2017, 12, 09),
                    Description = "Холодильник с морозильником Indesit DF 5180 E",
                    UserId = list.ElementAt(0),
                    Code = ToGuid(new DateTime(2017, 12, 09)).ToString(),
                    Price = 820,
                    Image = "\\images\\fridges\\indesit\\big\\DF 5180 E.jpg",
                    ImageB = "\\images\\fridges\\indesit\\small\\DF 5180 Es.jpg",
                    IsExist = true,
                    SoldOut = false
                },

                new Good
                {
                    Name = "DFE 4160 S",
                    BrandId = 8,
                    CategoryId = 2,
                    ReceiptDate = new DateTime(2017, 12, 10),
                    Description = "Холодильник с морозильником Indesit DFE 4160 S",
                    UserId = list.ElementAt(1),
                    Code = ToGuid(new DateTime(2017, 12, 10)).ToString(),
                    Price = 820,
                    Image = "\\images\\fridges\\indesit\\big\\DFE 4160 S.jpg",
                    ImageB = "\\images\\fridges\\indesit\\small\\DFE 4160 Ss.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "GA-B389 SMQZ",
                    BrandId = 7,
                    CategoryId = 2,
                    ReceiptDate = new DateTime(2017, 12, 10),
                    Description = "полный No Frost, электронное управление, класс A++, полезный объём: "+
                    "261 л (182 + 79 л), инверторный компрессор, 59.5x64.3x173.7 см, нержавеющая "+
                    "сталь",
                    UserId = list.ElementAt(1),
                    Code = ToGuid(new DateTime(2017, 12, 10)).ToString(),
                    Price = 820,
                    Image = "\\images\\fridges\\LG\\big\\GA-B389 SMQZ.jpg",
                    ImageB = "\\images\\fridges\\LG\\small\\GA-B389 SMQZs.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "GA-B499TGBM",
                    BrandId = 7,
                    CategoryId = 2,
                    ReceiptDate = new DateTime(2017, 12, 11),
                    Description = "полный No Frost, электронное управление, класс A++, полезный "+
                    "объём: 360 л (255 + 105 л), инверторный компрессор, зона свежести, 59.5x66.8x200 см, черный",
                    UserId = list.ElementAt(1),
                    Code = ToGuid(new DateTime(2017, 12, 11)).ToString(),
                    Price = 1820,
                    Image = "\\images\\fridges\\LG\\big\\GA-B499TGBM.jpg",
                    ImageB = "\\images\\fridges\\LG\\small\\GA-B499TGBMs.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "RB-33J3420BC",
                    BrandId = 5,
                    CategoryId = 2,
                    ReceiptDate = new DateTime(2017, 11, 01),
                    Description = "полный No Frost, электронное управление, класс A+, полезный объём: 328 л (230 + 98 л), инверторный компрессор, 59.5x66.8x185 см, черный",
                    UserId = list.ElementAt(3),
                    Code = ToGuid(new DateTime(2017, 11, 01)).ToString(),
                    Price = 1220,
                    Image = "\\images\\fridges\\samsung\\big\\RB-33J3420BC.jpg",
                    ImageB = "\\images\\fridges\\samsung\\small\\RB-33J3420BCs.jpg"
                },
                new Good
                {
                    Name = "RB-41 J7861S4",
                    BrandId = 5,
                    CategoryId = 2,
                    ReceiptDate = new DateTime(2017, 11, 01),
                    Description = "полный No Frost, электронное управление, класс A+, полезный объём: 410 л (280 + 130 л), инверторный компрессор, зона свежести, 59.5x65x201.7 см, нержавеющая сталь",
                    UserId = list.ElementAt(3),
                    Code = ToGuid(new DateTime(2017, 11, 01)).ToString(),
                    Price = 2000,
                    Image = "\\images\\fridges\\samsung\\big\\RB-41 J7861S4.jpg",
                    ImageB = "\\images\\fridges\\samsung\\small\\RB-41-J7861S4s.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "60У1010-00",
                    BrandId = 4,
                    CategoryId = 3,
                    ReceiptDate = new DateTime(2017, 10, 01),
                    Description = "автоматическая стиральная машина, загрузка до 6 кг, отжим 1000 об/мин, глубина 40.7 см, энергопотребление A++, защита от протечек, 16 программ",
                    UserId = list.ElementAt(3),
                    Code = ToGuid(new DateTime(2017, 10, 01)).ToString(),
                    Price = 634,
                    Image = "\\images\\Washers\\atlant\\big\\60У1010-00.jpg",
                    ImageB = "\\images\\Washers\\atlant\\small\\60У1010-00s.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "СМА 50У82",
                    BrandId = 4,
                    CategoryId = 3,
                    ReceiptDate = new DateTime(2017, 10, 09),
                    Description = "автоматическая стиральная машина, загрузка до 5 кг, отжим 800 об/мин, глубина 40 см, энергопотребление A, 15 программ",
                    UserId = list.ElementAt(2),
                    Code = ToGuid(new DateTime(2017, 10, 09)).ToString(),
                    Price = 400,
                    Image = "\\images\\Washers\\atlant\\big\\СМА 50У82.jpg",
                    ImageB = "\\images\\Washers\\atlant\\small\\СМА-50У82s.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "BWSB 61051",
                    BrandId = 8,
                    CategoryId = 3,
                    ReceiptDate = new DateTime(2017, 12, 09),
                    Description = "автоматическая стиральная машина, загрузка до 6 кг, отжим 1000 об/мин, глубина 43 см, энергопотребление A+, защита от протечек, 16 программ",
                    UserId = list.ElementAt(2),
                    Code = ToGuid(new DateTime(2017, 12, 09)).ToString(),
                    Price = 470,
                    Image = "\\images\\Washers\\indesit\\big\\BWSB 61051.jpg",
                    ImageB = "\\images\\Washers\\indesit\\small\\BWSB-61051s.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "ITW D 51052 W",
                    BrandId = 8,
                    CategoryId = 3,
                    ReceiptDate = new DateTime(2017, 12, 22),
                    Description = "автоматическая стиральная машина, загрузка до 5 кг, отжим 1000 об/мин, глубина 60 см, энергопотребление A++, 14 программ",
                    UserId = list.ElementAt(2),
                    Code = ToGuid(new DateTime(2017, 12, 22)).ToString(),
                    Price = 470,
                    Image = "\\images\\Washers\\indesit\\big\\ITW D 51052 W.jpg",
                    ImageB = "\\images\\Washers\\indesit\\small\\ITW-D-51052-Ws.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "F4H9VS2S",
                    BrandId = 7,
                    CategoryId = 3,
                    ReceiptDate = new DateTime(2017, 12, 24),
                    Description = "автоматическая стиральная машина, с паром, загрузка до 9 кг, отжим 1400 об/мин, глубина 56 см, энергопотребление A, прямой привод, 14 программ",
                    UserId = list.ElementAt(3),
                    Code = ToGuid(new DateTime(2017, 12, 24)).ToString(),
                    Price = 1200,
                    Image = "\\images\\Washers\\LG\\big\\F4H9VS2S.jpg",
                    ImageB = "\\images\\Washers\\LG\\big\\F4H9VS2Ss.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "FH-2G6WDS3",
                    BrandId = 7,
                    CategoryId = 3,
                    ReceiptDate = new DateTime(2017, 12, 19),
                    Description = "автоматическая стиральная машина, с паром, загрузка до 6.5 кг, отжим 1200 об/мин, глубина 44 см, энергопотребление A, прямой привод, 13 программ",
                    UserId = list.ElementAt(3),
                    Code = ToGuid(new DateTime(2017, 12, 19)).ToString(),
                    Price = 700,
                    Image = "\\images\\Washers\\LG\\big\\FH-2G6WDS3.jpg",
                    ImageB = "\\images\\Washers\\LG\\small\\FH-2G6WDS3s.jpg",
                    IsExist = true,
                    SoldOut = false

                },
                new Good
                {
                    Name = "WW60J30G0LW",
                    BrandId = 5,
                    CategoryId = 3,
                    ReceiptDate = new DateTime(2017, 12, 23),
                    Description = "автоматическая стиральная машина, с паром, загрузка до 6 кг, отжим 1000 об/мин, глубина 45 см, энергопотребление A, 12 программ",
                    UserId = list.ElementAt(1),
                    Code = ToGuid(new DateTime(2017, 12, 23)).ToString(),
                    Price = 800,
                    Image = "\\images\\Washers\\samsung\\big\\WW60J30G0LW.jpg",
                    ImageB = "\\images\\Washers\\samsung\\small\\WW60J30G0LWs.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "WW70K62E69W",
                    BrandId = 5,
                    CategoryId = 3,
                    ReceiptDate = new DateTime(2017, 12, 25),
                    Description = "автоматическая стиральная машина, с паром, загрузка до 7 кг, отжим 1200 об/мин, глубина 45 см, энергопотребление A, 14 программ",
                    UserId = list.ElementAt(1),
                    Code = ToGuid(new DateTime(2017, 12, 25)).ToString(),
                    Price = 1200,
                    Image = "\\images\\Washers\\samsung\\big\\WW70K62E69W.jpg",
                    ImageB = "\\images\\Washers\\samsung\\small\\WW70K62E69Ws.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "ПГ 2100-00",
                    BrandId = 12,
                    CategoryId = 4,
                    ReceiptDate = new DateTime(2017, 12, 30),
                    Description = "газовая панель + газовый духовой шкаф, поверхность: эмалированная сталь, щиток, управление механическое, без таймера, размеры (ШхГхВ): 50x55x85 см",
                    UserId = list.ElementAt(2),
                    Code = ToGuid(new DateTime(2017, 12, 30)).ToString(),
                    Price = 230,
                    Image = "\\images\\gas stoves\\cezaris\\big\\ПГ 2100-00.JPG",
                    ImageB = "\\images\\gas stoves\\cezaris\\small\\ПГ-2100-00s.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "ПГ 3000-00",
                    BrandId = 12,
                    CategoryId = 4,
                    ReceiptDate = new DateTime(2017, 12, 30),
                    Description = "газовая панель + газовый духовой шкаф, поверхность: эмалированная сталь, щиток, управление механическое, без таймера, размеры (ШхГхВ): 60x60x85 см",
                    UserId = list.ElementAt(2),
                    Code = ToGuid(new DateTime(2017, 12, 30)).ToString(),
                    Price = 280,
                    Image = "\\images\\gas stoves\\cezaris\\big\\ПГ 3000-00.jpg",
                    ImageB = "\\images\\gas stoves\\cezaris\\small\\ПГ-3000-00s.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "3200-08",
                    BrandId = 6,
                    CategoryId = 4,
                    ReceiptDate = new DateTime(2017, 12, 27),
                    Description = "газовая панель + газовый духовой шкаф, поверхность: эмалированная сталь, щиток, управление механическое, без таймера, размеры (ШхГхВ): 50x57x85 см",
                    UserId = list.ElementAt(0),
                    Code = ToGuid(new DateTime(2017, 12, 27)).ToString(),
                    Price = 270,
                    Image = "\\images\\gas stoves\\gefest\\big\\3200-08.jpg",
                    ImageB = "\\images\\gas stoves\\gefest\\small\\3200-08s.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "5100-02 0010 (Т2К)",
                    BrandId = 6,
                    CategoryId = 4,
                    ReceiptDate = new DateTime(2017, 12, 26),
                    Description = "газовая панель + газовый духовой шкаф, поверхность: эмалированная сталь, крышка из эмалированной стали, управление механическое, без таймера, размеры (ШхГхВ): 50x58.5x85 см",
                    UserId = list.ElementAt(0),
                    Code = ToGuid(new DateTime(2017, 12, 26)).ToString(),
                    Price = 420,
                    Image = "\\images\\gas stoves\\gefest\\big\\5100-02 0010 (Т2К).jpg",
                    ImageB = "\\images\\gas stoves\\gefest\\small\\5100-02-0010-(Т2К)s.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "FCGW52024",
                    BrandId = 13,
                    CategoryId = 4,
                    ReceiptDate = new DateTime(2017, 12, 04),
                    Description = "газовая панель + газовый духовой шкаф, поверхность: эмалированная сталь, крышка из закалённого стекла, управление механическое, без таймера, размеры (ШхГхВ): 50x60x85 см",
                    UserId = list.ElementAt(3),
                    Code = ToGuid(new DateTime(2017, 12, 04)).ToString(),
                    Price = 533,
                    Image = "\\images\\gas stoves\\hansa\\big\\FCGW52024.jpg",
                    ImageB = "\\images\\gas stoves\\hansa\\small\\FCGW52024s.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "FCGX52077",
                    BrandId = 13,
                    CategoryId = 4,
                    ReceiptDate = new DateTime(2017, 12, 04),
                    Description = "газовая панель + газовый духовой шкаф, поверхность: нержавеющая сталь, крышка из эмалированной стали, управление механическое, без таймера, размеры (ШхГхВ): 50x60x85 см",
                    UserId = list.ElementAt(3),
                    Code = ToGuid(new DateTime(2017, 12, 04)).ToString(),
                    Price = 565,
                    Image = "\\images\\gas stoves\\hansa\\big\\FCGX52077.jpg",
                    ImageB = "\\images\\gas stoves\\hansa\\small\\FCGX52077s.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "KN 1G2 (W)",
                    BrandId = 8,
                    CategoryId = 4,
                    ReceiptDate = new DateTime(2017, 11, 04),
                    Description = "газовая панель + газовый духовой шкаф, поверхность: эмалированная сталь, крышка из закалённого стекла, управление механическое, таймер, размеры (ШхГхВ): 50x50x85 см",
                    UserId = list.ElementAt(2),
                    Code = ToGuid(new DateTime(2017, 11, 04)).ToString(),
                    Price = 670,
                    Image = "\\images\\gas stoves\\indesit\\big\\KN 1G2 (W).jpg",
                    ImageB ="\\images\\gas stoves\\indesit\\small\\KN-1G2-(W)s.jpg",
                    IsExist = true,
                    SoldOut = false
                },

                new Good
                {
                    Name = "KN1G20(W)",
                    BrandId = 8,
                    CategoryId = 4,
                    ReceiptDate = new DateTime(2017, 11, 05),
                    Description = "газовая панель + газовый духовой шкаф, поверхность: эмалированная сталь, управление механическое, таймер, размеры (ШхГхВ): 50x50x85 см",
                    UserId = list.ElementAt(2),
                    Code = ToGuid(new DateTime(2017, 11, 05)).ToString(),
                    Price = 480,
                    Image = "\\images\\gas stoves\\indesit\\big\\KN1G20(W).jpg",
                    ImageB = "\\images\\gas stoves\\indesit\\small\\KN1G20(W)s.jpg",
                    IsExist = true,
                    SoldOut = false
                },

                new Good
                {
                    Name = "FTXB-20C",
                    BrandId = 10,
                    CategoryId = 5,
                    ReceiptDate = new DateTime(2018, 01, 05),
                    Description = "сплит-система, настенный блок, инвертор, мощность охлаждения 2 кВт, мощность обогрева 2.5 кВт, шум 21-39 дБ",
                    UserId = list.ElementAt(1),
                    Code = ToGuid(new DateTime(2018, 01, 05)).ToString(),
                    Price = 1480,
                    Image = "\\images\\conditioners\\daikin\\big\\FTXB-20C.jpg",
                    ImageB = "\\images\\conditioners\\daikin\\small\\FTXB-20Cs.jpg",
                    IsExist = true,
                    SoldOut = false

                },
                new Good
                {
                    Name = "FTYN25GX",
                    BrandId = 10,
                    CategoryId = 5,
                    ReceiptDate = new DateTime(2018, 01, 05),
                    Description = "сплит-система, настенный блок, мощность охлаждения 2.5 кВт, мощность обогрева 2.85 кВт, шум 38 дБ",
                    UserId = list.ElementAt(1),
                    Code = ToGuid(new DateTime(2018, 01, 05)).ToString(),
                    Price = 1080,
                    Image = "\\images\\conditioners\\daikin\\big\\FTYN25GX.jpg",
                    ImageB = "\\images\\conditioners\\daikin\\small\\FTYN25GXs.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "KSW-07H4",
                    BrandId = 11,
                    CategoryId = 5,
                    ReceiptDate = new DateTime(2018, 01, 10),
                    Description = "сплит-система, настенный блок, мощность охлаждения 2.2 кВт, мощность обогрева 2.3 кВт, шум 27-37 дБ",
                    UserId = list.ElementAt(3),
                    Code = ToGuid(new DateTime(2018, 01, 10)).ToString(),
                    Price = 600,
                    Image = "\\images\\conditioners\\komatsu\\big\\KSW-07H4.jpg",
                    ImageB = "\\images\\conditioners\\komatsu\\small\\KSW-07H4s.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "KSW-12H5",
                    BrandId = 11,
                    CategoryId = 5,
                    ReceiptDate = new DateTime(2018, 01, 10),
                    Description = "сплит-система, настенный блок, мощность охлаждения 3.21 кВт, мощность обогрева 3.48 кВт, шум 32-42 дБ",
                    UserId = list.ElementAt(3),
                    Code = ToGuid(new DateTime(2018, 01, 10)).ToString(),
                    Price = 600,
                    Image = "\\images\\conditioners\\komatsu\\big\\KSW-12H5.jpg",
                    ImageB = "\\images\\conditioners\\komatsu\\small\\KSW-12H5s.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "CS-TZ35TKEW",
                    BrandId = 9,
                    CategoryId = 5,
                    ReceiptDate = new DateTime(2018, 01, 12),
                    Description = "сплит-система, настенный блок, инвертор, мощность охлаждения 3.5 кВт, мощность обогрева 4 кВт, шум 20-42 дБ",
                    UserId = list.ElementAt(0),
                    Code = ToGuid(new DateTime(2018, 01, 12)).ToString(),
                    Price = 1600,
                    Image = "\\images\\conditioners\\panasonic\\big\\CS-TZ35TKEW.jpg",
                    ImageB = "\\images\\conditioners\\panasonic\\small\\CS-TZ35TKEWs.jpg",
                    IsExist = true,
                    SoldOut = false
                },
                new Good
                {
                    Name = "CS-W7NKD",
                    BrandId = 9,
                    CategoryId = 5,
                    ReceiptDate = new DateTime(2018, 01, 12),
                    Description = "сплит-система, настенный блок, мощность охлаждения 2.24 кВт, мощность обогрева 2.38 кВт",
                    UserId = list.ElementAt(0),
                    Code = ToGuid(new DateTime(2018, 01, 12)).ToString(),
                    Price = 800,
                    Image = "\\images\\conditioners\\panasonic\\big\\CS-W7NKD.jpg",
                    ImageB = "\\images\\conditioners\\panasonic\\small\\CS-W7NKDs.jpg",
                    IsExist = true,
                    SoldOut = false
                },

            };
            return goods;
        }      
        
        private static Guid ToGuid(DateTime d)
        {
            DateTime dateTime = d;
            Guid guid = Guid.NewGuid();
            string guidEnd = guid.ToString().Substring(guid.ToString().IndexOf("-"), guid.ToString().Length - guid.ToString().IndexOf("-"));
            string guidStart = dateTime.Day.ToString() + dateTime.Month.ToString() + dateTime.Year.ToString() /*+ dateTime.Hour.ToString() + dateTime.Minute.ToString()+ dateTime.Second.ToString() + value.ToString()*/;
            guidStart = guidStart.PadRight(8, '0');
            guid = new Guid(guidStart + guidEnd);
            return guid;
        }

        private static List<ShopUser> CreateShopUser()
        {
            PasswordHasher<ShopUser> hasher = new PasswordHasher<ShopUser>();
            ShopUser uAdmin = new ShopUser
            {
                Email = "admin@mail.ru",
                NormalizedEmail = "ADMIN@MAIL.RU",
                PhoneNumber = "+375296223223",
                UserName = "admin",
                NormalizedUserName = "ADMIN",
                SecurityStamp = Guid.NewGuid().ToString(),
                LockoutEnabled = true
                
            };

            List<ShopUser> users = new List<ShopUser>()
            {
                new ShopUser
                {
                    Email = "111@mail.ru",
                    NormalizedEmail ="111@MAIL.RU",
                    PhoneNumber="+375291445565",
                    UserName = "one",
                    NormalizedUserName = "ONE",
                    SecurityStamp = Guid.NewGuid().ToString(),
                    LockoutEnabled = true
                },
                new ShopUser
                {
                    Email = "222@mail.ru",
                    NormalizedEmail ="222@MAIL.RU",
                    PhoneNumber="+375291445533",
                    UserName = "two",
                    NormalizedUserName = "TWO",
                    SecurityStamp = Guid.NewGuid().ToString(),
                    LockoutEnabled = true
                },
                new ShopUser
                {
                    Email = "333@mail.ru",
                    NormalizedEmail ="333@MAIL.RU",                    
                    PhoneNumber ="+375291212565",
                    UserName = "three",
                    NormalizedUserName = "THREE",
                    SecurityStamp = Guid.NewGuid().ToString(),
                    LockoutEnabled = true
                },
                new ShopUser
                {
                    Email = "444@mail.ru",
                    NormalizedEmail ="444@MAIL.RU",                    
                    PhoneNumber ="+375291213335",
                    UserName = "four",
                    NormalizedUserName = "FOUR",
                    SecurityStamp = Guid.NewGuid().ToString(),
                    LockoutEnabled = true
                }
            };
            foreach (var xx in users)
            {
                xx.PasswordHash = hasher.HashPassword(xx, "qqqq");
            }

            uAdmin.PasswordHash = hasher.HashPassword(uAdmin, "Admin");
            users.Add(uAdmin);

            return users;
        }

        private static List<UserProfile> CreateUserProfile()
        {
            List<UserProfile> users = new List<UserProfile>()
            {
                new UserProfile
                {
                    Address = "г.Гомель, ул.Советская, 23-14",
                    Basket = new Basket { Goods = new List<Good>()},
                    Firstname ="Иван",
                    LastName ="Иванов",
                    ShopUser = CreateShopUser()[0],                    
                },
                new UserProfile
                {
                    Address = "г.Гомель, ул.Свиридова, 33-15",
                    Basket = new Basket { Goods = new List<Good>()},
                    Firstname ="Петя",
                    LastName ="Петров",
                    ShopUser = CreateShopUser()[1],
                },
                new UserProfile
                {
                    Address = "г.Гомель, ул.Кожара, 143-114",
                    Basket = new Basket { Goods = new List<Good>()},
                    Firstname ="Саша",
                    LastName ="Сашин",
                    ShopUser = CreateShopUser()[2]
                },
                new UserProfile
                {
                    Address = "г.Гомель, ул.Ильича, 153-19",
                    Basket = new Basket { Goods = new List<Good>()},
                    Firstname ="Степа",
                    LastName ="Степанов",
                    ShopUser = CreateShopUser()[3]
                },
                new UserProfile
                {
                    Address = "г.Гомель, ул.Пушкина, 53-10",
                    Basket = new Basket { Goods = new List<Good>()},
                    Firstname ="Олег",
                    LastName ="Админов",
                    ShopUser = CreateShopUser()[4]
                },
            };

            return users;
        }
        
        # endregion
    }
}
