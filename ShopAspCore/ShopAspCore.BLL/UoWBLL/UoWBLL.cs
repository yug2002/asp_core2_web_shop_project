﻿using AutoMapper;
using ShopAspCore.BLL.DataTransferObject;
using ShopAspCore.BLL.Interfaces;
using ShopAspCore.BLL.MyMapper;
using ShopAspCore.BLL.Services;
using ShopAspCore.DLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShopAspCore.BLL.UoWBLL
{
    public class UoWBLL : IUoWBLL
    {
        private IUnitOfWork db;
        static UoWBLL()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<CategoryProfile>();
                cfg.AddProfile<CategoryBrandProfile>();
                cfg.AddProfile<GoodProfile>();
                cfg.AddProfile<BrandProfile>();
                cfg.AddProfile<GoodFunc>();
                cfg.AddProfile<BasketProfile>();                
                cfg.AddProfile<UserProfileProfile>();
            });
        }
        public UoWBLL(IUnitOfWork context)
        {
            db = context;
        }
        public IService<GoodDTO> Goods => new GoodsService(db);

        public IService<BrandDTO> Brands => new BrandsService(db);

        public IService<CategoryDTO> Categories => new CategoriesService(db);

        public IService<BasketDTO> Baskets => new BasketService(db);

        public IService<UserProfileDTO> UserProfiles => new UserProfileService(db);

        
    }
}
