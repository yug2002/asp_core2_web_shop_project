﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShopAspCore.BLL.DataTransferObject
{
   public class CategoryBrandDTO
    {
        public int BrandId { get; set; }
        public BrandDTO Brand { get; set; }
        public int CategoryId { get; set; }
        public CategoryDTO Category { get; set; }
    }
}

