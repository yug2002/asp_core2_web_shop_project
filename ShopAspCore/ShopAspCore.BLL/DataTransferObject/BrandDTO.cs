﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShopAspCore.BLL.DataTransferObject
{
    public class BrandDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<CategoryBrandDTO> CategoryBrands { get; } = new List<CategoryBrandDTO>();
        //public List<CategoryDTO> Categories { get; set; }
    }
}
