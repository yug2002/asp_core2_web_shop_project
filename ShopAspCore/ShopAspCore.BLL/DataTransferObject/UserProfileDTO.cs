﻿using ShopAspCore.DLL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShopAspCore.BLL.DataTransferObject
{
    public class UserProfileDTO
    {
        public string Id { get; set; }
        public string LastName { get; set; }
        public string Firstname { get; set; }
        public string Address { get; set; }

        public string ShopUserForeignKey { get; set; }       
        public string Email { get; set; }
        public string Login { get; set; }
        public string PhoneNumber { get; set; }
        public string Role { get; set; }
        public BasketDTO Basket { get; set; }       
        
    }
}
