﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShopAspCore.BLL.DataTransferObject
{
    public class GoodDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }

        public DateTime ReceiptDate { get; set; }
        public DateTime? DateOfSale { get; set; }

        public CategoryDTO Category { get; set; }

        public string Code { get; set; }        
        
        public BrandDTO Brand { get; set; }

        public string Description { get; set; }

        public string Image { get; set; }

        public string ImageB { get; set; }

        public string UserId { get; set; }
        //public UserProfileDTO User { get; set; }       

        public bool SoldOut { get; set; }        
        
        public int? BasketId { get; set; }
        //public BasketDTO Basket { get; set; }
    }
}
