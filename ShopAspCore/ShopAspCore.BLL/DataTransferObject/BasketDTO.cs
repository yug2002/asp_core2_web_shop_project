﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShopAspCore.BLL.DataTransferObject
{
    public class BasketDTO
    {
        public int Id { get; set; }

        public List<GoodDTO> Goods { get; set; } = new List<GoodDTO>();       

        public UserProfileDTO UserProfile { get; set; }
    }
}
