﻿using AutoMapper;
using ShopAspCore.BLL.DataTransferObject;
using ShopAspCore.DLL.Entities;
using ShopAspCore.DLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ShopAspCore.BLL.Services
{
    public class BrandsService : IService<BrandDTO>
    {
        private IUnitOfWork context;

        public BrandsService(IUnitOfWork context)
        {
            this.context = context;
        }
        public async Task CreateAsync(BrandDTO item)
        {
            var brand = Mapper.Map<BrandDTO, Brand>(item);
            await context.Brands.Create(brand);
            await context.SaveAsync();
        }

        public async Task DeleteAsync(int id)
        {
            await context.Brands.Delete(id);
            await context.SaveAsync();
        }

        public async Task<IEnumerable<BrandDTO>> FindAsync(Expression<Func<BrandDTO, bool>> predicate)
        {
            var _predicate = Mapper.Map<Expression<Func<Brand, bool>>>(predicate);
            var list = Mapper.Map<IEnumerable<Brand>, IEnumerable<BrandDTO>>(await context.Brands.Find(_predicate));
            return list;
        }

        public async Task<IQueryable<BrandDTO>> GetAllAsync()
        {
            var source = (await context.Brands.GetAll()).ToList();
            var all = Mapper.Map<IEnumerable<Brand>, IEnumerable<BrandDTO>>(source);
            for (int i = 0; i < all.Count(); i++)
            {
                var cb = source.ElementAt(i).CategoryBrands;
                var cbdto = Mapper.Map<List<CategoryBrandDTO>>(cb);
                foreach (var xx in cbdto)
                {
                    all.ElementAt(i).CategoryBrands.Add(xx);
                }
            }
            return all.AsQueryable();
        }

        public async Task<BrandDTO> GetAsync(int id)
        {
            var item = Mapper.Map<Brand, BrandDTO>(await context.Brands.Get(id));
            return item;
        }

        public Task<BrandDTO> GetAsync(string id)
        {
            throw new NotImplementedException();
        }

        public Task<BrandDTO> GetAsync(Expression<Func<BrandDTO, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task UpdateAsync(BrandDTO item)
        {
            var good = Mapper.Map<BrandDTO, Brand>(item);
            await context.Brands.Update(good);
            await context.SaveAsync();
        }

    }
}
