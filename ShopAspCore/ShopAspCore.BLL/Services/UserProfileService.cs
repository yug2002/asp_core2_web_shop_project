﻿using AutoMapper;
using ShopAspCore.BLL.DataTransferObject;
using ShopAspCore.DLL.Entities;
using ShopAspCore.DLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ShopAspCore.BLL.Services
{
    public class UserProfileService : IService<UserProfileDTO>
    {
        private IUnitOfWork context;

        public UserProfileService(IUnitOfWork context)
        {
            this.context = context;
        }
        public async Task CreateAsync(UserProfileDTO item)
        {
            var user = Mapper.Map<UserProfileDTO, UserProfile>(item);
            await context.UserProfiles.Create(user);
            await context.SaveAsync();
        }

        public async Task DeleteAsync(int id)
        {
            await context.UserProfiles.Delete(id);
        }

        public async Task<IEnumerable<UserProfileDTO>> FindAsync(Expression<Func<UserProfileDTO, bool>> predicate)
        {
            var _predicate = Mapper.Map<Expression<Func<UserProfile, bool>>>(predicate);
            var list = Mapper.Map<IEnumerable<UserProfile>, IEnumerable<UserProfileDTO>>(await context.UserProfiles.Find(_predicate));
            return list;
        }        

        public async Task<IQueryable<UserProfileDTO>> GetAllAsync()
        {
            var source = (await context.UserProfiles.GetAll()).ToList();
            var all = Mapper.Map<IEnumerable<UserProfile>, IEnumerable<UserProfileDTO>>(source);
            return all.AsQueryable();
        }

        public Task<UserProfileDTO> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<UserProfileDTO> GetAsync(string id)
        {
            var user = Mapper.Map<UserProfile, UserProfileDTO>(await context.UserProfiles.Get(id));
            return user;            
        }

        public Task<UserProfileDTO> GetAsync(Expression<Func<UserProfileDTO, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task UpdateAsync(UserProfileDTO item)
        {
            var user = Mapper.Map<UserProfileDTO, UserProfile>(item);
            await context.UserProfiles.Update(user);            
        }        
    }
}
