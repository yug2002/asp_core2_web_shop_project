﻿using AutoMapper;
using ShopAspCore.BLL.DataTransferObject;
using ShopAspCore.BLL.Interfaces;
using ShopAspCore.BLL.MyMapper;
using ShopAspCore.DLL.Entities;
using ShopAspCore.DLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ShopAspCore.BLL.Services
{
    public class GoodsService : IService<GoodDTO>
    {
        private IUnitOfWork context;
        
        public GoodsService(IUnitOfWork context)
        {
            this.context = context;
        }

        public async Task CreateAsync(GoodDTO item)
        {
            //Mapper.Initialize(cfg => cfg.CreateMap<Good, GoodDTO>());
            var good = Mapper.Map<GoodDTO, Good>(item);
            await context.Goods.Create(good);
            
        }

        public async Task DeleteAsync(int id)
        {
            await context.Goods.Delete(id);
            await context.SaveAsync();
        }

        public async Task<IEnumerable<GoodDTO>> FindAsync(Expression<Func<GoodDTO, bool>> predicate)
        {            
            var exp = Mapper.Map<Expression<Func<Good, bool>>>(predicate); 
            var ll = (await context.Goods.Find(exp)).ToList();
            var l = Mapper.Map<IEnumerable<Good>, IEnumerable<GoodDTO>>(ll).ToList();
            return l;
        }

        public async Task<IQueryable<GoodDTO>> GetAllAsync()
        {
            
            var all = Mapper.Map<IEnumerable<Good>,IEnumerable<GoodDTO>>(await context.Goods.GetAll()).AsQueryable();
            return all;
        }

        public async Task<GoodDTO> GetAsync(int id)
        {
            //Mapper.Initialize(cfg => cfg.CreateMap<Good, GoodDTO>());
            var item = Mapper.Map<Good, GoodDTO>(await context.Goods.Get(id));
            return item;
        }

        public Task<GoodDTO> GetAsync(string id)
        {
            throw new NotImplementedException();
        }

        public Task<GoodDTO> GetAsync(Expression<Func<GoodDTO, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task UpdateAsync(GoodDTO item)
        {
            //Mapper.Initialize(cfg => cfg.CreateMap<Good, GoodDTO>());
            var good = Mapper.Map<GoodDTO, Good>(item);
            await context.Goods.Update(good);
            await context.SaveAsync();
            
        }
    }
}
