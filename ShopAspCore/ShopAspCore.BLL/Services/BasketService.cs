﻿using ShopAspCore.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using ShopAspCore.DLL.Entities;
using ShopAspCore.DLL.Access;
using ShopAspCore.DLL.Interfaces;
using ShopAspCore.BLL.DataTransferObject;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;

namespace ShopAspCore.BLL.Services
{
    public class BasketService : IService<BasketDTO>
    {
        private IUnitOfWork context;

        public BasketService(IUnitOfWork context)
        {
            this.context = context;
        }

        public async Task CreateAsync(BasketDTO item)
        {
            var basket = Mapper.Map<BasketDTO, Basket>(item);
            await context.Baskets.Create(basket);
            await context.SaveAsync();
        }

        public async Task DeleteAsync(int id)
        {
            await context.Baskets.Delete(id);
            await context.SaveAsync();
        }

        public async Task<IEnumerable<BasketDTO>> FindAsync(Expression<Func<BasketDTO, bool>> predicate)
        {
            var _predicate = Mapper.Map<Expression<Func<Basket, bool>>>(predicate);
            var list = Mapper.Map<IEnumerable<Basket>, IEnumerable<BasketDTO>>(await context.Baskets.Find(_predicate));
            return list;
        }

        public async Task<IQueryable<BasketDTO>> GetAllAsync()
        {
            var list = Mapper.Map<IEnumerable<Basket>, IEnumerable<BasketDTO>>(await context.Baskets.GetAll());
            return list.AsQueryable();
        }

        public async Task<BasketDTO> GetAsync(int id)
        {
            var item =Mapper.Map<Basket,BasketDTO>(await context.Baskets.Get(id));
            return item;
        }

        public Task<BasketDTO> GetAsync(string id)
        {
            throw new NotImplementedException();
        }

        public Task<BasketDTO> GetAsync(Expression<Func<BasketDTO, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task UpdateAsync(BasketDTO item)
        {
            var basket = Mapper.Map<BasketDTO, Basket>(item);
            await context.Baskets.Update(basket);
           
        }
    }
}
