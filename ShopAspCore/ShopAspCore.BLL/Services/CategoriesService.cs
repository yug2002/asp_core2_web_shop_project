﻿using AutoMapper;
using ShopAspCore.BLL.DataTransferObject;
using ShopAspCore.BLL.MyMapper;
using ShopAspCore.DLL.Entities;
using ShopAspCore.DLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ShopAspCore.BLL.Services
{
    public class CategoriesService : IService<CategoryDTO>
    {
        private IUnitOfWork context;
        
        public CategoriesService(IUnitOfWork context)
        {
            this.context = context;
        }
        public async Task CreateAsync(CategoryDTO item)
        {
            var category = Mapper.Map<CategoryDTO, Category>(item);
            await context.Categories.Create(category);
            await context.SaveAsync();
        }

        public async Task DeleteAsync(int id)
        {
            await context.Categories.Delete(id);
            await context.SaveAsync();
        }

        public async Task<IEnumerable<CategoryDTO>> FindAsync(Expression<Func<CategoryDTO, bool>> predicate)
        {
            var _predicate = Mapper.Map<Expression<Func<Category, bool>>>(predicate);
            var list = Mapper.Map<IQueryable<Category>, IQueryable<CategoryDTO>>(await context.Categories.Find(_predicate));
            return list;
        }

        public async Task<IQueryable<CategoryDTO>> GetAllAsync()
        {
            var source = (await context.Categories.GetAll()).ToList();            
            var all = Mapper.Map<IEnumerable<Category>, IEnumerable<CategoryDTO>>(source);
            for(int i = 0; i < all.Count(); i++)
            {
                var cb = source.ElementAt(i).CategoryBrands;
                var cbdto = Mapper.Map<List<CategoryBrandDTO>>(cb);                
                foreach(var xx in cbdto)
                {
                    all.ElementAt(i).CategoryBrands.Add(xx);
                }        
            }
            return all.AsQueryable();
        }

        public async Task<CategoryDTO> GetAsync(int id)
        {
            var item =  Mapper.Map<Category, CategoryDTO>(await context.Categories.Get(id));
            return item;
        }

        public Task<CategoryDTO> GetAsync(string id)
        {
            throw new NotImplementedException();
        }        

        public Task<CategoryDTO> GetAsync(Expression<Func<CategoryDTO, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task UpdateAsync(CategoryDTO item)
        {
            var category = Mapper.Map<CategoryDTO, Category>(item);
            await context.Categories.Update(category);
            await context.SaveAsync();
        }
    }
}
