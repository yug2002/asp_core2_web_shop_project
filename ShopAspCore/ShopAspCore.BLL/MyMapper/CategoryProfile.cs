﻿using AutoMapper;
using ShopAspCore.BLL.DataTransferObject;
using ShopAspCore.DLL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShopAspCore.BLL.MyMapper
{
    public class CategoryProfile:Profile
    {
        public CategoryProfile()
        {            
            
            CreateMap<Category, CategoryDTO>()
                .PreserveReferences()
                .ReverseMap();
        } 
    }
}
