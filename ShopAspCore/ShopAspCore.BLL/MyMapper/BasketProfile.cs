﻿using AutoMapper;
using ShopAspCore.BLL.DataTransferObject;
using ShopAspCore.DLL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShopAspCore.BLL.MyMapper
{
    public class BasketProfile:Profile
    {
        public BasketProfile()
        {
            CreateMap<Basket, BasketDTO>()
                .ReverseMap();
        }
    }
}
