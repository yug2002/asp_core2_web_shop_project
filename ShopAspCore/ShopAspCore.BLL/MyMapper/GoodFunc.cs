﻿using AutoMapper;
using ShopAspCore.BLL.DataTransferObject;
using ShopAspCore.DLL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShopAspCore.BLL.MyMapper
{
    public class GoodFunc:Profile
    {
        public GoodFunc()
        {
            CreateMap<Func<Good, bool>, Func<GoodDTO, bool>>()
                .ReverseMap();
        }
    }
}
