﻿using ShopAspCore.BLL.DataTransferObject;
using ShopAspCore.BLL.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShopAspCore.BLL.Interfaces
{
    public interface IUoWBLL
    {
        IService<GoodDTO> Goods { get; }
        IService<BrandDTO> Brands { get; }
        IService<CategoryDTO> Categories { get; }
        IService<BasketDTO> Baskets { get; }        
        IService<UserProfileDTO> UserProfiles { get; }
    }
}
