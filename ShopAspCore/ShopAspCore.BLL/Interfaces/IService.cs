﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ShopAspCore.BLL.Services
{
    public interface IService<T> where T:class 
    {
        Task<IQueryable<T>> GetAllAsync();
        Task<T> GetAsync(int id);
        Task<T> GetAsync(string id);
        Task<T> GetAsync(Expression<Func<T, Boolean>> predicate);
        Task<IEnumerable<T>> FindAsync(Expression<Func<T, Boolean>> predicate);
        Task CreateAsync(T item);
        Task UpdateAsync(T item);
        Task DeleteAsync(int id);
    }
}
