﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using ShopAspCore.DLL.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using ShopAspCore.WEB.Models.AccountViewModels;
using System.Security.Claims;
using ShopAspCore.WEB.Models.Interfaces;
using ShopAspCore.BLL.DataTransferObject;
using ShopAspCore.WEB.Filters;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ShopAspCore.WEB.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<ShopUser> _userManager;
        private readonly SignInManager<ShopUser> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ILogger _logger; 
        private readonly IGoodWebService _goodWebService;
        private readonly IUserProfileWebService _userProfileWebService;
        private readonly IBasketWebService _basketWebService;
        private readonly ICategoryWebService _categoryWebService;
        private readonly IBrandWebService _brandWebService;

        public AccountController(
            UserManager<ShopUser> userManager,
            SignInManager<ShopUser> signInManager, 
            RoleManager<IdentityRole> roleManager,
            ILoggerFactory loggerFactory,
            IGoodWebService goodWebService,
            IUserProfileWebService userProfileWebService,
            IBasketWebService basketWebService,
            ICategoryWebService categoryWebService,
            IBrandWebService brandWebService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _logger = loggerFactory.CreateLogger<AccountController>();
            _goodWebService = goodWebService;
            _userProfileWebService = userProfileWebService;
            _basketWebService = basketWebService;
            _categoryWebService = categoryWebService;
            _brandWebService = brandWebService;            
        }        

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login(string returnUrl = null)
        {
            // Clear the existing external cookie to ensure a clean login process

            //await AuthenticationHttpContextExtensions.SignOutAsync(_httpContext);

            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                var result = await _signInManager.PasswordSignInAsync(model.Login, model.Password, model.RememberMe, lockoutOnFailure: false);

                if (result.Succeeded)
                {
                    _logger.LogInformation(1, "User logged in.");
                    
                    HttpContext.Response.Cookies.Append("login", model.Login);
                    return RedirectToLocal(returnUrl);
                }
                if (result.IsLockedOut)
                {
                    _logger.LogWarning(2, "User account locked out.");
                    return View("Lockout");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    return View(model);
                }
            }

            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                var userRole = _roleManager.FindByNameAsync("User");
                var shopUser = new ShopUser
                {
                    UserName = model.Login,
                    Email = model.Email
                };
                var result = await _userManager.CreateAsync(shopUser, model.Password);
                if (result.Succeeded)
                {
                    var currentUser = _userManager.FindByEmailAsync(model.Email).Result;
                    if (currentUser == null)
                        return NotFound();

                    await _userManager.AddToRoleAsync(currentUser, "User");
                    await _signInManager.SignInAsync(shopUser, isPersistent: false);
                    _logger.LogInformation(3, "User created a new account with password.");
                    return RedirectToLocal(returnUrl);
                }
                else
                {
                    AddErrors(result);
                }

                var user = new UserProfileDTO
                {
                    Address = model.Address,
                    Firstname = model.FirstName,
                    LastName = model.LastName,
                    ShopUserForeignKey = shopUser.Id,
                    Basket = new BasketDTO { Goods = new List<GoodDTO>() }                    
                };
                await _userProfileWebService.Create(user);
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            HttpContext.Response.Cookies.Delete("login");
            _logger.LogInformation(4, "User logged out.");
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }
        
        [CustomExceptionFilter]        
        public async Task<IActionResult> ShowBasket()
        {
            if (_userProfileWebService.IsUserLogin())
            {
                var goods = await _basketWebService.GetGoodFromBasketByCurrentUser();
                return View(goods);
            }           
            return RedirectToAction(nameof(Register), "Account");
        }

        [HttpPost]        
        public async Task<int> InBasket(int idGood)
        {   
            var c = await _userProfileWebService.AddGoodInBasket(idGood);   
            return c;
        }

        public async Task<IActionResult> Cabinet()
        {
            var goods = await _userProfileWebService.GetCommitedPurchase();
            return View();
        } 

        public async Task<IActionResult> MyPurchasesInCabinet(int page = 1)
        {
            var goods = await _userProfileWebService.GetCommitedPurchase();
            var pageObj = _goodWebService.GetObjectForPagination(goods, page);
            return Json(pageObj);
        }

        public async Task<IActionResult> MyGoodsInCabinet(int page = 1)
        {
            var goods = await _goodWebService.GetOwnGoods();
            var pageObj = _goodWebService.GetObjectForPagination(goods, page);
            return Json(pageObj);
        }

        [HttpGet]
        public async Task<IActionResult> GetCountGoodsInBasket()
        {
            List<GoodDTO> listOfGoods = await _userProfileWebService.GetGoodsFromBasket();
            return Json(listOfGoods.Count);
        }

        public async Task<IActionResult> MakePurchase(int Id)
        {
            var g = await _userProfileWebService.Buy(Id);
            return View(g);
        }

        public async Task<IActionResult> Refuse(int Id)
        {
            await _goodWebService.Refuse(Id);
            return RedirectToAction(nameof(ShowBasket), "Account");
        }

        public async Task<IActionResult> GetAllCategories()
        {
            var cats = await _categoryWebService.GetAll();
            return Json(cats);
        }

        public async Task<IActionResult> GetAllBrands()
        {
            var brands = await _brandWebService.GetAll();
            return Json(brands);
        }

        [HttpGet]
        public IActionResult AccessDenied()
        {
            return View();
        }

        #region(help)

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        private async Task<ShopUser> GetCurrentUserAsync()
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            return user;
        }
        private string GetIdUserFromClaims()
        {
            var id = User.Claims.FirstOrDefault(u => u.Type == ClaimTypes.NameIdentifier)?.Value;
            return id;
        }
        #endregion

    }
}
