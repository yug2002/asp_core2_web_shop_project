﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ShopAspCore.WEB.Models;
using Microsoft.EntityFrameworkCore;
using ShopAspCore.WEB.TagHelpers;
using System.Threading;
using ShopAspCore.WEB.Models.Interfaces;
using ShopAspCore.BLL.DataTransferObject;
using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Http;
using ShopAspCore.WEB.StaticHelpers;

namespace ShopAspCore.WEB.Controllers
{
    public class HomeController : Controller
    {        
        private readonly IGoodWebService _goodWebService;
        private readonly IBrandWebService _brandWebService;
        private readonly ICategoryWebService _categoryWebService;
        private readonly IStringLocalizer<HomeController> _localizer;
        private readonly string cult="";

        public HomeController(
            IGoodWebService goodWebService,
            IBrandWebService brandWebService, 
            ICategoryWebService categoryWebService,
            IStringLocalizer<HomeController> localizer)
        {           
            _goodWebService = goodWebService;
            _brandWebService = brandWebService;
            _categoryWebService = categoryWebService;
            _localizer = localizer;
            cult = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
            HttpHelper.HttpContext.Response.Cookies.Append("culture", cult);
        }


        public async Task<IActionResult> Index()
        {
            var ss = await _categoryWebService.GetAll();
            var s = _localizer["На главную"];
            return  View(ss);
        }

        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );
            HttpContext.Response.Cookies.Append("culture", culture);
            return LocalRedirect(returnUrl);
        }

        public async Task<IActionResult> GetGoodsByCategory(int id = 0,int page = 1)
        {
            
            var source = await _goodWebService.GetGoodsByCategoryId(id);
            var obj = GetObjectForPagination(source, page);
            return Json(obj);
        }

        public async Task<IActionResult> GetGoods(int page = 1)
        {
            var source = await _goodWebService.GetAll();             
            var obj = GetObjectForPagination(source, page);
            return Json(obj);
        }

        public async Task<IActionResult> GetGoodsByBrandIdCategoryId(int idBrand, int idCategory, int page = 1)
        {            
            var source = await _goodWebService.GetGoodsByBrandIdCategoryId(idBrand, idCategory);
            var obj = GetObjectForPagination(source, page);
            return Json(obj);
        }

        public async Task<IActionResult> GetGoodsByBrandId(int idBrand, int page = 1)
        {            
            var source = await _goodWebService.GetGoodsByBrandId(idBrand);
            var obj = GetObjectForPagination(source, page);            
            return Json(obj);
        }

        public async Task<IActionResult> GetBrands()
        {
            var brands = await _brandWebService.GetAll();
            return Json(brands);
        }
        
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        #region Helper        

        private IndexGoodsViewModel GetObjectForPagination(IQueryable<GoodDTO> goods, int page)
        {
            int pageSize = 4;
            var count = goods.Count();
            var items = goods.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            PageViewModel pageViewModel = new PageViewModel(count, page, pageSize);
            IndexGoodsViewModel indexGoodsViewModel = new IndexGoodsViewModel
            {
                Goods = items,
                PageViewModel = pageViewModel
            };
            return indexGoodsViewModel;
        }
        #endregion
    }
}
