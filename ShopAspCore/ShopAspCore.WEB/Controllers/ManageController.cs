﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ShopAspCore.BLL.DataTransferObject;
using ShopAspCore.DLL.Entities;
using ShopAspCore.WEB.Models;
using ShopAspCore.WEB.Models.Interfaces;
using ShopAspCore.WEB.TagHelpers;

namespace ShopAspCore.WEB.Controllers
{
    
    [Authorize]
    public class ManageController : Controller
    {
        private readonly UserManager<ShopUser> _userManager;
        private readonly SignInManager<ShopUser> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ILogger _logger;
        private readonly IGoodWebService _goodWebService;
        private readonly IUserProfileWebService _userProfileWebService;
        private readonly IBasketWebService _basketWebService;
        private readonly ICategoryWebService _categoryWebService;

        public ManageController(
            UserManager<ShopUser> userManager,
            SignInManager<ShopUser> signInManager,
            RoleManager<IdentityRole> roleManager,
            ILoggerFactory loggerFactory,
            IGoodWebService goodWebService,
            IUserProfileWebService userProfileWebService,
            IBasketWebService basketWebService,
            ICategoryWebService categoryWebService
            )
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _logger = loggerFactory.CreateLogger<ManageController>();
            _goodWebService = goodWebService;
            _userProfileWebService = userProfileWebService;
            _basketWebService = basketWebService;
            _categoryWebService = categoryWebService;

        }
        public IActionResult Index()
        {
            //var list = await GetAllProfileUsersByUsers(await GetUserProfiles());
            return View();
        }

        public async Task<IActionResult> ListUsers(int page = 1)
        {
            int pagesize = 4;
            
            var listUserProfiles = _userProfileWebService.GetUserProfilesByUsers(_userManager);

            var count = (await listUserProfiles).Count();
            var items = (await listUserProfiles).Skip((page - 1) * pagesize).Take(pagesize).ToList();

            PageViewModel pageViewModel = new PageViewModel(count, page, pagesize);
            IndexUsersViewModel usersViewModel = new IndexUsersViewModel
            {
                PageViewModel = pageViewModel,
                Users = items
            };

            return View(usersViewModel);
        }

        public async Task<IActionResult> ListGoods(int page = 1)
        {
            int pagesize = 6;
            var goods = _goodWebService.GetAllForAdm();

            var count = (await goods).Count();
            var items = (await goods).Skip((page - 1) * pagesize).Take(pagesize).ToList();

            PageViewModel pageViewModel = new PageViewModel(count, page, pagesize);
            IndexGoodsViewModel goodsViewModel = new IndexGoodsViewModel
            {
                PageViewModel = pageViewModel,
                Goods = items
            };
            return View(goodsViewModel);
        }

        public async Task<IActionResult> ShowItemDetails(int Id)
        {
            var good = await _goodWebService.GetGood(Id);
            return View(good);
        }

        public async Task<IActionResult> GetStatusOfGood(int Id)
        {
            var s = await _goodWebService.GetStatusOfGoodByIdGood(Id);
            return Json(s);
        }

        public async Task<IActionResult> GetBuyerOfGoodByIdBasket(int Id)
        {           
             var lastname = Id != 0 ? (await _userProfileWebService.GetUserProfileByIdBasket(Id)).LastName : "-";
             return Json(lastname);
        }

        public async Task<IActionResult> GetCustomerData(string idUser)
        {
            if (idUser == null)
            {
                return RedirectToAction(nameof(Index), "Manage");
            }
            var ux = await _userProfileWebService.GetCustomerDataByShopUser(_userManager, idUser); 
            return View(ux);
        }
        public async Task<IActionResult> GetCustomerDataByBasketId(int BasketId)
        {
            var ux = await _userProfileWebService.GetCustomerDataByUserManagerAndBasketId(_userManager, BasketId);
            return View(ux);
        }

        public async Task<IActionResult> GetUserNameById(string Id)
        {
            var us = (await _userProfileWebService.GetUserProfileById(Id)).LastName;
            return Json(us);
        }

        //public async Task<IActionResult> GetAllCategories()
        //{
        //    var cat = await _categoryWebService.GetAll();
        //    return Json(cat);
        //}

        #region[helpers]

        private async Task<ShopUser> GetShopUser(string userId)
        {            
            var ddd = await _userManager.FindByIdAsync(userId);
            return ddd;
        }



        private async Task<List<ShopUser>> GetUserProfiles()
        {
            var list = (await _userManager.GetUsersInRoleAsync("User")).ToList();
            return list;
        }
        
        #endregion
    }
}