﻿using ShopAspCore.BLL.DataTransferObject;
using ShopAspCore.DLL.Entities;
using ShopAspCore.WEB.TagHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopAspCore.WEB.Models
{
    public class IndexGoodsViewModel
    {        
        public PageViewModel PageViewModel { get; set; }
        public IEnumerable<GoodDTO> Goods { get; set; }
    }
}
