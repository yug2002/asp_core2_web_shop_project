﻿using Microsoft.AspNetCore.Http;
using ShopAspCore.BLL.DataTransferObject;
using ShopAspCore.BLL.Interfaces;
using ShopAspCore.DLL.Entities;
using ShopAspCore.WEB.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using ShopAspCore.WEB.StaticHelpers;

namespace ShopAspCore.WEB.Models.Services
{
    public class UserProfileWebService : IUserProfileWebService
    {
        private IUoWBLL context;
        private IBasketWebService basketWeb;
        private IGoodWebService goodWeb;


        public UserProfileWebService(IUoWBLL context, IBasketWebService basketWeb, IGoodWebService goodWeb)
        {
            this.context = context;
            this.goodWeb = goodWeb;
            this.basketWeb = basketWeb;

        }

        public async Task Create(UserProfileDTO user)
        {
            await context.UserProfiles.CreateAsync(user);
        }

        public async Task<UserProfileDTO> GetUserProfileByIdShopUser(string id)
        {
            var user = (await context.UserProfiles.FindAsync(xx => xx.ShopUserForeignKey == id)).FirstOrDefault();
            return user;
        }

        public async Task<List<UserProfileDTO>> GetUserProfilesByUsers(UserManager<ShopUser> manager)
        {
            List<UserProfileDTO> usprList = (await context.UserProfiles.GetAllAsync()).ToList();
            var shopUsersList = manager.Users;
            foreach(var u in shopUsersList)
            {
                var uspr = usprList.Find(xx => xx.ShopUserForeignKey == u.Id);
                uspr.Login = u.UserName;                
                uspr.PhoneNumber = u.PhoneNumber;
                uspr.Role = (await manager.GetRolesAsync(u)).FirstOrDefault();
                uspr.Email = u.Email;                
            }
            return usprList;
        }
        public static async Task<UserProfileDTO> GetUserProfileByIdShopUser(string id, IUoWBLL context)
        {
            var user = (await context.UserProfiles.FindAsync(xx => xx.ShopUserForeignKey == id)).FirstOrDefault();
            return user;
        }

        public async Task<IQueryable<GoodDTO>> GetCommitedPurchase()
        {
            var us = await GetUserProfileByIdShopUser(GetIdUserFromClaims());
            var id = (await basketWeb.GetBasketByUserId(us.Id)).Id;
            var goods = await goodWeb.GetMyCommitedPurchases(id);
            return goods;
        }

        public async Task<int> AddGoodInBasket(int idGood)
        {            
            var currUser = await GetUserProfileByIdShopUser(GetIdUserFromClaims());
            var bs = await context.Baskets.GetAllAsync();
            var basketsUs = bs.Where(xx => xx.UserProfile.Id.Equals(currUser.Id)).FirstOrDefault();             
            await goodWeb.PutGoodInBasket(basketsUs,idGood);
            var count = bs.Count();
            return count;
        }
        
        public async Task<GoodDTO> Buy(int idGood)
        {
            var list = await basketWeb.GetGoodFromBasketByCurrentUser();
            var currGood = list.Find(xx => xx.Id == idGood);
            currGood.DateOfSale = DateTime.Now;
            await goodWeb.Update(currGood);
            return currGood; 

        }

        public async Task Update(UserProfileDTO user)
        {
             await context.UserProfiles.UpdateAsync(user);
        }

        public async Task<List<GoodDTO>> GetGoodsFromBasket()
        {
            var prof = await GetUserProfileByIdShopUser(GetIdUserFromClaims());
            var Basket = await basketWeb.GetBasketByUserId(prof.Id);
            var listOfGoods = (await goodWeb.GetGoodsByBasketId(Basket.Id)).Where(xx=>xx.DateOfSale==null).ToList();
            return listOfGoods;
        }

        public async Task<IQueryable<UserProfileDTO>> GetAllUserProfiles()
        {
            var uss = await context.UserProfiles.GetAllAsync();
            return uss;
        }

        public bool IsUserLogin()
        {
            var res = GetIdUserFromClaims() != null;
            return res;
        }

        public async Task<UserProfileDTO> GetUserProfileById(string id)
        {
            var us = await context.UserProfiles.GetAsync(id);
            return us;
        }

        public async Task<UserProfileDTO> GetUserProfileByIdBasket(int Id)
        {
            var us = (await context.UserProfiles.FindAsync(xx => xx.Basket.Id == Id)).FirstOrDefault();
            return us;
        }
        private async Task<UserProfileDTO> GetUserProfileByUserManagerAndId(UserManager<ShopUser> manager, string id)
        {
            var uspr = await context.UserProfiles.GetAsync(id);
            var shU = await manager.FindByIdAsync(uspr.ShopUserForeignKey);
            var role = (await manager.GetRolesAsync(shU)).FirstOrDefault();
            //var uspr = await GetUserProfileByIdShopUser(id);
            uspr.Email = shU.Email;
            uspr.Login = shU.UserName;
            uspr.PhoneNumber = shU.PhoneNumber;
            uspr.Role = role;
            return uspr;
        }
        private async Task<UserProfileDTO> GetUserProfileByUserManagerAndBasketId(UserManager<ShopUser> manager, int id)
        {
            var uspr = await GetUserProfileByIdBasket(id);
            var shU = await manager.FindByIdAsync(uspr.ShopUserForeignKey);
            var role = (await manager.GetRolesAsync(shU)).FirstOrDefault();
            //var uspr = await GetUserProfileByIdShopUser(id);
            uspr.Email = shU.Email;
            uspr.Login = shU.UserName;
            uspr.PhoneNumber = shU.PhoneNumber;
            uspr.Role = role;
            return uspr;
        }

        public async Task<UserProfileDTO> GetCustomerDataByUserManagerAndBasketId(UserManager<ShopUser> manager, int id)
        {
            var uspr = await GetUserProfileByUserManagerAndBasketId(manager, id);
            return uspr;

        }
        public async Task<UserProfileDTO> GetCustomerDataByShopUser(UserManager<ShopUser> manager, string id)
        {
            var u = await GetUserProfileByUserManagerAndId(manager, id);
            return u;
        }

        private string GetIdUserFromClaims()
        {
            var id = HttpHelper.HttpContext.User.Claims.FirstOrDefault(xx => xx.Type == ClaimTypes.NameIdentifier)?.Value;
            return id;
        }

       
    }
}
