﻿using ShopAspCore.BLL.DataTransferObject;
using ShopAspCore.BLL.Interfaces;
using ShopAspCore.WEB.Models.Interfaces;
using ShopAspCore.WEB.StaticHelpers;
using ShopAspCore.WEB.TagHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ShopAspCore.WEB.Models.Services
{
    public class GoodWebService : IGoodWebService
    {
        private readonly IUoWBLL db;
        
        public GoodWebService(IUoWBLL context)
        {
            db = context;            
        }
        public async Task<IQueryable<GoodDTO>> GetAll()
        {            
            var list = (await db.Goods.GetAllAsync()).Where(xx => xx.SoldOut == false);
            var l = await GetNotOwnGoods(list);
            return list;
        }

        public async Task<IQueryable<GoodDTO>> GetAllForAdm()
        {
            var list = await db.Goods.GetAllAsync();            
            return list;
        }



        //получаем весь товар кроме собственного (для зарегистрированных) 
        private async Task<IQueryable<GoodDTO>> GetNotOwnGoods(IQueryable<GoodDTO> listOfAllGoods)
        {
            var prof = await GetUserProfile();
            var l = listOfAllGoods.Where(uu => uu.UserId != (prof == null ? "" : prof.Id));
            return l;
        }
        //получаем собственный товар (для зарегистрированных) 
        private async Task<IQueryable<GoodDTO>> GetOwnGoods(IQueryable<GoodDTO> listOfAllGoods)
        {
            var prof = await GetUserProfile();
            var l = listOfAllGoods.Where(uu => uu.UserId == (prof == null ? "" : prof.Id));
            return l;
        }
        public IndexGoodsViewModel GetObjectForPagination(IQueryable<GoodDTO> goods, int page)
        {
            int pageSize = 5;
            var count = goods.Count();
            var items = goods.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            PageViewModel pageViewModel = new PageViewModel(count, page, pageSize);
            IndexGoodsViewModel indexGoodsViewModel = new IndexGoodsViewModel
            {
                Goods = items,
                PageViewModel = pageViewModel
            };
            return indexGoodsViewModel;
        }

        public async Task<IQueryable<GoodDTO>> GetOwnGoods()
        {
            var list = await GetOwnGoods(await GetAllForAdm());
            return list;
        }

        public async Task<GoodDTO> MakePurchase(int idGood)
        {                       
            var good = await GetGood(idGood);            
            good.BasketId = null;            
            await Update(good);
            return good;
        }
        public async Task Refuse(int idGood)
        {
            var good = await GetGood(idGood);
            good.SoldOut = false; 
            good.BasketId = null;
            await Update(good);            
        }

        public async Task<GoodDTO> GetGood(int id)
        {
            return await db.Goods.GetAsync(id);

        }

        public async Task<IQueryable<GoodDTO>> GetGoodsByBrandId(int id)
        {
            var prof = await GetUserProfile();
            var list = (await db.Goods.FindAsync(xx => xx.Brand.Id == id)).AsQueryable();
            var currl = await GetNotOwnGoods(list);
            return currl;
        }

        public async Task<IQueryable<GoodDTO>> GetGoodsByBrandIdCategoryId(int brandId, int categoryId)
        {
            Expression<Func<GoodDTO, bool>> exp = dto => dto.Category.Id == categoryId && dto.Brand.Id == brandId;
            var list = (await db.Goods.FindAsync(exp)).AsQueryable();
            var currl = await GetNotOwnGoods(list);
            return currl;
        }

        public async Task PutGoodInBasket(BasketDTO basket, int idGood)
        {
            var good = await db.Goods.GetAsync(idGood);
            good.BasketId = basket.Id;
            good.SoldOut = true;
            await Update(good);
        }

        public async Task<IQueryable<GoodDTO>> GetGoodsByCategoryId(int id)
        {
            Expression<Func<GoodDTO, bool>> exp = dto => dto.Category.Id == id;
            var list = await db.Goods.FindAsync(exp);
            var currl = await GetNotOwnGoods(list.AsQueryable());
            return currl;
        }


        public async Task<List<GoodDTO>> GetGoodsByBasketId(int id)
        {
            var list = (await db.Goods.FindAsync(xx => xx.BasketId == id)).ToList();
            return list;
        }

        public async Task<IQueryable<GoodDTO>> GetMyCommitedPurchases(int basketId)
        {
            var list = await GetGoodsByBasketId(basketId);
            var currList = (list.FindAll(xx => xx.SoldOut == true && xx.DateOfSale != null)).AsQueryable();
            return currList;
        }

        public async Task<string> GetStatusOfGoodByIdGood(int Id)
        {
            var good = await db.Goods.GetAsync(Id);
            var status = (new Status(good)).getStatus();
            return status;

        }

        public async Task Update(GoodDTO good)
        {
            await db.Goods.UpdateAsync(good);
        }

        private async Task<UserProfileDTO> GetUserProfile()
        {
            var usId = GetIdUserFromClaims();
            var prof = await UserProfileWebService.GetUserProfileByIdShopUser(usId, db);
            return prof;
        }

        private string GetIdUserFromClaims()
        {
            var id = HttpHelper.HttpContext.User.Claims.FirstOrDefault(xx => xx.Type == ClaimTypes.NameIdentifier)?.Value;
            return id;
        }

        
    }
}
