﻿using ShopAspCore.BLL.DataTransferObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopAspCore.WEB.Models.Services
{
    public class Status
    {
        private const string sold = "продан";
        private const string notSold = "не продан";
        private const string inBasket = "в корзине"; 

        private GoodDTO good;
        public Status(GoodDTO good)
        {
            this.good = good;
        }

        public string getStatus()
        {
            if(good.SoldOut == true && good.DateOfSale != null)
            {
                return sold;
            }
            else if(good.SoldOut == true && good.DateOfSale == null)
            {
                return inBasket;
            }
            return notSold;
        }


    }
}
