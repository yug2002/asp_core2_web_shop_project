﻿using ShopAspCore.BLL.DataTransferObject;
using ShopAspCore.BLL.Interfaces;
using ShopAspCore.BLL.UoWBLL;
using ShopAspCore.WEB.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopAspCore.WEB.Models.Services
{
    public class BrandWebService : IBrandWebService
    {
        private IUoWBLL context;
        public BrandWebService(IUoWBLL context)
        {
            this.context = context;
        }
        public async Task<IQueryable<BrandDTO>> GetAll()
        {
            var brands =await context.Brands.GetAllAsync();

            return brands;
        }
        public async Task<IQueryable<BrandDTO>> GetBrandsByIdCategory(int id)
        {
            var list = await context.Brands.FindAsync(xx => xx.CategoryBrands.Any(bb => bb.CategoryId == id));
            return list.AsQueryable();
        }
    }
}
