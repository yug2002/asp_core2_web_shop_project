﻿using ShopAspCore.BLL.DataTransferObject;
using ShopAspCore.BLL.Interfaces;
using ShopAspCore.BLL.UoWBLL;
using ShopAspCore.WEB.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopAspCore.WEB.Models.Services
{
    public class CategoryWebService : ICategoryWebService
    {
        private IUoWBLL context;

        public CategoryWebService(IUoWBLL context)
        {
            this.context = context;
        }
        public async Task<IQueryable<CategoryDTO>> GetAll()
        {
            var categories = await context.Categories.GetAllAsync();
            return categories;
        }

        public Task<IQueryable<CategoryDTO>> GetCategoriesByBrandId(int id)
        {
            throw new NotImplementedException();
        }

        public Task<CategoryDTO> GetCategory(int id)
        {
            throw new NotImplementedException();
        }
    }
}
