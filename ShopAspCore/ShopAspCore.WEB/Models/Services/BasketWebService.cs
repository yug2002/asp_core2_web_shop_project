﻿using ShopAspCore.BLL.DataTransferObject;
using ShopAspCore.BLL.Interfaces;
using ShopAspCore.WEB.Models.Interfaces;
using ShopAspCore.WEB.StaticHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ShopAspCore.WEB.Models.Services
{
    public class BasketWebService : IBasketWebService
    {
        private IUoWBLL context;

        public BasketWebService(IUoWBLL context)
        {
            this.context = context;
        }

        public async Task<IQueryable<BasketDTO>> GetAll()
        {
            return await context.Baskets.GetAllAsync();
        }

        public async Task Update(BasketDTO basket)
        {
            await context.Baskets.UpdateAsync(basket);
        }

        public async Task<BasketDTO> GetBasketByUserId(string userId)
        {
            var bs = await GetAll();
            var b = bs.Where(xx => xx.UserProfile.Id.Equals(userId)).FirstOrDefault();
            return b;
        }

        public async Task<List<GoodDTO>> GetGoodFromBasketByCurrentUser()
        {
            var user = await UserProfileWebService.GetUserProfileByIdShopUser(GetIdUserFromClaims(), context);
            var basket = (await context.Baskets.FindAsync(xx=>xx.UserProfile.Id == user.Id)).FirstOrDefault();
            var goods = await context.Goods.FindAsync(xx => xx.BasketId == basket.Id && xx.DateOfSale==null);
            return goods.ToList();
        }

        private string GetIdUserFromClaims()
        {
            var id = HttpHelper.HttpContext.User.Claims.FirstOrDefault(xx => xx.Type == ClaimTypes.NameIdentifier)?.Value;
            return id;
        }

    }
}
