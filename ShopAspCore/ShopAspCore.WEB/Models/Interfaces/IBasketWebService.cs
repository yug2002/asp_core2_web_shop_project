﻿using ShopAspCore.BLL.DataTransferObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopAspCore.WEB.Models.Interfaces
{
    public interface IBasketWebService
    {
        Task<IQueryable<BasketDTO>> GetAll();
        Task Update(BasketDTO basket);
        Task<BasketDTO> GetBasketByUserId(string userId);
        Task<List<GoodDTO>> GetGoodFromBasketByCurrentUser();
    }
}
