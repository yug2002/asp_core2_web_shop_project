﻿using Microsoft.AspNetCore.Identity;
using ShopAspCore.BLL.DataTransferObject;
using ShopAspCore.DLL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopAspCore.WEB.Models.Interfaces
{
    public interface IUserProfileWebService
    {
        Task<UserProfileDTO> GetUserProfileByIdShopUser(string id);
        Task<UserProfileDTO> GetUserProfileById(string id);
        Task<int> AddGoodInBasket(int idGood);
        Task Update(UserProfileDTO user);
        Task Create(UserProfileDTO user);
        Task<List<GoodDTO>> GetGoodsFromBasket();
        Task<IQueryable<GoodDTO>> GetCommitedPurchase();
        Task<GoodDTO> Buy(int idGood);
        bool IsUserLogin();
        Task<List<UserProfileDTO>> GetUserProfilesByUsers(UserManager<ShopUser> manager);
        Task<UserProfileDTO> GetUserProfileByIdBasket(int Id);
        Task<UserProfileDTO> GetCustomerDataByShopUser(UserManager<ShopUser> manager, string id);
        Task<UserProfileDTO> GetCustomerDataByUserManagerAndBasketId(UserManager<ShopUser> manager, int id);

    }
}
