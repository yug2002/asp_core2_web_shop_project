﻿using ShopAspCore.BLL.DataTransferObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopAspCore.WEB.Models.Interfaces
{
    public interface IBrandWebService
    {
        Task<IQueryable<BrandDTO>> GetAll();
        Task<IQueryable<BrandDTO>> GetBrandsByIdCategory(int id);
    }
}
