﻿using ShopAspCore.BLL.DataTransferObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopAspCore.WEB.Models.Interfaces
{
    public interface IGoodWebService
    {
        Task<IQueryable<GoodDTO>> GetAll();
        Task<IQueryable<GoodDTO>> GetAllForAdm();
        Task<IQueryable<GoodDTO>> GetGoodsByCategoryId(int id);
        Task<IQueryable<GoodDTO>> GetGoodsByBrandId(int id);
        Task<IQueryable<GoodDTO>> GetGoodsByBrandIdCategoryId(int brandId, int categoryId);
        Task PutGoodInBasket(BasketDTO basket, int idGood);
        Task<List<GoodDTO>> GetGoodsByBasketId(int id);
        Task<GoodDTO> GetGood(int id);
        Task Update(GoodDTO good);
        Task<GoodDTO> MakePurchase(int idGood);
        Task<IQueryable<GoodDTO>> GetMyCommitedPurchases(int basketId);
        Task Refuse(int idGood);
        Task<string> GetStatusOfGoodByIdGood(int Id);
        Task<IQueryable<GoodDTO>> GetOwnGoods();
        IndexGoodsViewModel GetObjectForPagination(IQueryable<GoodDTO> goods, int page);
    }
}
