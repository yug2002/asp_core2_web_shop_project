﻿using ShopAspCore.BLL.DataTransferObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopAspCore.WEB.Models.Interfaces
{
    public interface ICategoryWebService
    {
        Task<IQueryable<CategoryDTO>> GetAll();
        Task<IQueryable<CategoryDTO>> GetCategoriesByBrandId(int id);
        Task<CategoryDTO> GetCategory(int id);
    }
}
