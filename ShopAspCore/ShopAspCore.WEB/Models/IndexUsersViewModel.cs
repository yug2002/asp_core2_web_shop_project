﻿using ShopAspCore.BLL.DataTransferObject;
using ShopAspCore.WEB.TagHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopAspCore.WEB.Models
{
    public class IndexUsersViewModel
    {
        public PageViewModel PageViewModel { get; set; }
        public IEnumerable<UserProfileDTO> Users { get; set; }
    }
}
