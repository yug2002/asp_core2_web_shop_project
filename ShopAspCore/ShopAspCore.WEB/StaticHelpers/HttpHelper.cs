﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopAspCore.WEB.StaticHelpers
{
    public static class HttpHelper
    {
        private static IHttpContextAccessor accessor;
        public static void Configure(IHttpContextAccessor httpContextAccessor)
        {
            accessor = httpContextAccessor;
        }

        public static HttpContext HttpContext => accessor.HttpContext;

    }
}
