﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ShopAspCore.BLL.Interfaces;
using ShopAspCore.BLL.Services;
using ShopAspCore.BLL.UoWBLL;
using ShopAspCore.DLL.Access;
using ShopAspCore.DLL.Context;
using ShopAspCore.DLL.Entities;
using ShopAspCore.DLL.Interfaces;
using ShopAspCore.WEB.Models.Interfaces;
using ShopAspCore.WEB.Models.Services;
using ShopAspCore.WEB.StaticHelpers;
using System.Globalization;

namespace ShopAspCore.WEB
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;            
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            
            services.AddApplicationInsightsTelemetry(Configuration);

            string connection = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<ShopContext>(options=>options.UseSqlServer(connection));

            
            services.AddScoped<IUoWBLL, UoWBLL>();
            services.AddScoped<IGoodWebService, GoodWebService>();
            services.AddScoped<IBrandWebService, BrandWebService>();
            services.AddScoped<ICategoryWebService, CategoryWebService>();
            services.AddScoped<IBasketWebService, BasketWebService>();            
            services.AddScoped<IUserProfileWebService, UserProfileWebService>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddLocalization(options => options.ResourcesPath = "Resources");
           
            
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddAuthenticationCore();
            services.AddAuthorization();
            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 2;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
            });
            services.AddIdentity<ShopUser, IdentityRole>()
                .AddEntityFrameworkStores<ShopContext>()
                .AddDefaultTokenProviders();

            services
                .AddMvc()
                .AddViewLocalization()
                .AddDataAnnotationsLocalization();

            services.Configure<RequestLocalizationOptions>(options =>
            {
                var supportedCultures = new[]
                {
                    new CultureInfo("ru"),                    
                    new CultureInfo("be"),
                    new CultureInfo("en")
                };

                options.DefaultRequestCulture = new RequestCulture("ru");
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IApplicationLifetime applicationLifetime)
        {
            //get HttpContext in my application
            HttpHelper.Configure(app.ApplicationServices.GetRequiredService<IHttpContextAccessor>());

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            //app.UseApplicationInsightsRequestTelemetry();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();

                using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
                {
                    if (!serviceScope.ServiceProvider.GetService<ShopContext>().AllMigrationsApplied())
                    {
                        serviceScope.ServiceProvider.GetService<ShopContext>().Database.Migrate();
                        serviceScope.ServiceProvider.GetService<ShopContext>().EnsureSeedData();
                    }
                }
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            var supportedCultures = new[]
            {
                new CultureInfo("en-US"),
                new CultureInfo("en"),
                new CultureInfo("ru"),
                new CultureInfo("ru-RU"),
                new CultureInfo("be")

            };
            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture("ru"),
                SupportedCultures = supportedCultures,
                SupportedUICultures = supportedCultures
            });

            var locOptions = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(locOptions.Value);
            //app.UseApplicationInsightsExceptionTelemetry();

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
