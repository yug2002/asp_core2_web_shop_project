﻿$(document).ready(function () {

    var divRight = $("#right");   

    $.get('/home/GetGoods', null, function (data) {
        $(divRight).append('<div class="col-sm-12 row-goods"></div>');

        AppendGoods(data['goods']); 

        var divPag = $("#div-wrap-pagination");
        divPag.empty();
        $(divPag).append('<div id="div-prev" class="pagin"></div>');
        $(divPag).append('<div id="div-curr"></div>');
        $(divPag).append('<div id="div-next" class="pagin"></div>');

        AppendPagination(data['pageViewModel']);        
    });

    $('.col-goods').on('click', ".pagin", function () {
        var pageEnter = +$(this).text();
        if (pageEnter !== 0) {
            var divPag = $("#div-wrap-pagination");
            $.get('/home/GetGoods', { page: pageEnter }, function (data) {
                $(".row-goods").empty();                
                var rowGoods = $(".row-goods");
                AppendGoods(data['goods']);
                CorrectPagination(data['pageViewModel']);    
            });
        }
        return false;
    });
});