﻿$(document).ready(function () {

    function makeCounter() {        
        function counter() {
            counter.currentCount++;
        };
        counter.currentCount = 0;
        return counter;
    };
    
    var counter = makeCounter();

    $('.row-goods').on('click', '.my-goods', function () {
        $('.my-all').empty();
        add_header();
        counter.currentCount = 0;
        var tab_my_goods = $('.my-goods');
        
        light_tabs('.my-goods');        

        $.get('/account/MyGoodsInCabinet', null, function (paginObject) {
           
            $('.my-all-goods').empty();
            var j = 0;
            var goods = paginObject['goods'];

            $(goods).each(function (i) {
                counter();                
                $(".my-all-goods").append("<div id='" + i + "' class='row item-goods cabinet-goods'></div>");
                var div = $('#' + i);
                div.append("<div class='col-md-2 commit-purchase'>" + counter.currentCount + "</div>");
                div.append("<div class='col-md-2 commit-purchase'>" + $.date(goods[i].receiptDate) + "</div>");
                div.append("<div class='col-md-3 commit-purchase'>" + goods[i].name + "</div>");
                div.append("<div class='col-md-3 commit-purchase'>" + goods[i].brand.name + "</div>");
                div.append("<div class='col-md-2 commit-purchase'>" + goods[i].price + "</div>");
            });

            var divPag = $("#div-wrap-pagination");
            divPag.empty();
            $(divPag).append('<div id="div-prev" class="pagin_goods"></div>');
            $(divPag).append('<div id="div-curr"></div>');
            $(divPag).append('<div id="div-next" class="pagin_goods"></div>');

            AppendPagination(paginObject['pageViewModel']);            
        });
    });

    $('.row-goods').on('click', ".pagin_goods", function () {
        var pageEnter = +$(this).text();    
        if (pageEnter !== 0) {
            var divPag = $("#div-wrap-pagination");
            $.get('/account/MyGoodsInCabinet', { page: pageEnter }, function (paginObject) {
                $(".my-all-goods").empty(); 
                var goods = paginObject['goods'];
                var paginObj = paginObject['pageViewModel'];
                if (!flag_goods) {
                    length = goods.length;
                    flag_goods = true;
                }                            
                var pageNumber = paginObj.pageNumber;
                var res = (length * pageNumber) - length;    
                counter.currentCount = res;

                $(goods).each(function (i) {                    
                    $(".my-all-goods").append("<div id='" + i + "' class='row item-goods cabinet-goods'></div>");
                    var div = $('#' + i);                   
                    counter();
                    div.append("<div class='col-md-2 commit-purchase'>" + counter.currentCount + "</div>");
                    div.append("<div class='col-md-2 commit-purchase'>" + $.date(goods[i].receiptDate) + "</div>");
                    div.append("<div class='col-md-3 commit-purchase'>" + goods[i].name + "</div>");
                    div.append("<div class='col-md-3 commit-purchase'>" + goods[i].brand.name + "</div>");
                    div.append("<div class='col-md-2 commit-purchase'>" + goods[i].price + "</div>");
                });
                CorrectPagination(paginObject['pageViewModel']);
            });
            
        }
        return false;
    });    
})