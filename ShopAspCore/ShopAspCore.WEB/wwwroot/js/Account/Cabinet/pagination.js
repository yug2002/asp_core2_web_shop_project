﻿function AppendPagination(item) {

    var divPrev = $('#div-prev');
    var divCurr = $('#div-curr');
    var divNext = $('#div-next');

    $(divCurr).text(item.pageNumber);
    if (item.hasNextPage === true) {
        $(divNext).text(item.pageNumber + 1);
    }
    else {
        $(divNext).text('');
    }

    return false;
}

function CorrectPagination(item) {
    var divPrev = $('#div-prev');
    var divCurr = $('#div-curr');
    var divNext = $('#div-next');

    divPrev.text(item.pageNumber - 1);
    divCurr.text(item.pageNumber);
    divNext.text(item.pageNumber + 1);

    if (item.pageNumber === item.totalPages) {
        divNext.text('');
        return false;
    }
    else if (item.pageNumber === 1) {
        divPrev.text('');
        return false;
    }
}