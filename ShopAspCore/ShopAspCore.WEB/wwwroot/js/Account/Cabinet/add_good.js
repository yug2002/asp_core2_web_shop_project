﻿$(document).ready(function () {
    $('.row-goods').on('click', '.add-goods', function () {
        light_tabs('.add-goods');
        $('#div-wrap-pagination').empty();
        $('.my-all').empty();
        $('.my-all-goods').empty();

        $('.my-all').append('<form id="add-good-form"></form>');

        $('#add-good-form').append('<div class="form-group row f-add"></div>');
        $('.f-add').append('<label id="label-inputCateg" for="inputCateg" class="col-md-2 col-form-label">Категория</label>');
        $('.f-add').append('<div class="col-md-10 cs-f-add"></div>');
        $('.cs-f-add').append('<select id="inputCateg" class="form-control"></select>');
        $('#inputCateg').append('<option selected>Choose...</option>')
        $.get('/account/GetAllCategories', null, function (categories) {
            $(categories).each(function (i) {
                $('#inputCateg').append('<option>' + categories[i].name + '</option>');
            });
        });
        $('.f-add').append('<label id="label-inputBrand" for="inputBrand" class="col-md-2 col-form-label">Брэнд</label>');
        $('.f-add').append('<div class="col-md-10 bs-f-add"></div>');
        $('.bs-f-add').append('<select id="inputBrand" class="form-control"></select>');
        $('#inputBrand').append('<option selected>Choose...</option>');
        $.get('/account/GetAllBrands', null, function (brands) {
            $(brands).each(function (i) {
                $('#inputBrand').append('<option>' + brands[i].name + '</option>');
            });
        });
    });
})
