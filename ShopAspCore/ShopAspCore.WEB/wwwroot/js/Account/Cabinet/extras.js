﻿var tab_color_active = 'white';
var tab_back_color_active = '#FF0000';
var tab_color_simple = '#000000';
var tab_back_color_simple = '#5CB85C';
var flag_goods = false;
var flag_purchase = false;
var length = 0;

function light_tabs(_class) {
    $('.tabs').not(_class).css('color', tab_color_simple);
    $('.tabs').not(_class).css('background-color', tab_back_color_simple);
    $(_class).css('color', tab_color_active);
    $(_class).css('background-color', tab_back_color_active);
}