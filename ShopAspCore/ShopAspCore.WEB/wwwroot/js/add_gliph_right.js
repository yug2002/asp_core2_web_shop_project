﻿$(document).ready(function () {

    $('.panel-heading').on('click', '.panel-title', function () {

        var pb = $('.panel-body');
        pb.each(function (i, elem) {
            $(elem).css("background", "#fff");
        });

        var findDiv = $(this).find('.divCat').find('span.glyphicon');
        var p_h = $(this).parent();
        if (p_h.hasClass('ph')) {
            p_h.removeClass('ph');
            p_h.css("border-color", "red");
            p_h.css("color", "black");
        }
        else {
            p_h.addClass('ph');
        }
        var href = $(this).children().attr('href');
        var divPTs = $('.panel-title').not(this);

        if (findDiv.hasClass('glyphicon-menu-right')) {
            $(findDiv).removeClass('glyphicon-menu-right');
            $(findDiv).addClass('glyphicon-menu-down');
            divPTs.each(function (i, elem) {
                var ph = "";
                if ($(this).children().attr('href') !== href) {
                    var divS = $(this).find('.divCat').find('span.glyphicon');
                    $(divS).removeClass('glyphicon-menu-down');
                    $(divS).addClass('glyphicon-menu-right');
                    var div = $(this).parent();
                    div.css("border-color", "#000");
                    div.css("color", "white");
                    div.css("background", "-webkit-linear-gradient(top, #999999  0%, #222222 100%)");
                    $(this).parent().addClass('ph');
                }
            });
        }
        else {
            $(findDiv).removeClass('glyphicon-menu-down');
            $(findDiv).addClass('glyphicon-menu-right');
        }
        return true;
    });

    $('.panel').on('mouseover', '.ph', function () {
        $(this).css("border-color", "#000");
        $(this).css("color", "#000");
        $(this).css("background", "-webkit-linear-gradient(top, #222222  0%, #999999 100%)");
    });

    $('.panel').on('mouseout', '.ph', function () {
        $(this).css("border-color", "#000");
        $(this).css("color", "white");
        $(this).css("background", "-webkit-linear-gradient(top, #999999  0%, #222222 100%)");
    });

    $('.collapse').on('click', '.panel-body', function () {
        $(this).css("background", "red");
        var pb = $('.panel-body').not(this);
        pb.each(function (i, elem) {
            $(elem).css("background", "#fff");
        });
    });
});