﻿$(document).ready(function () {
    var idUser = $('.col-seller').data('id');
    var idGood = + $('.col-md-10-status').data('id');
    var idBasket = +$('.col-md-10-buyer').data('id');

    $.get("/manage/GetUserNameById", { Id: idUser }, function (lastname) {
        $('#a-seller').text(lastname);
    });


    $.get("/manage/GetStatusOfGood", { Id: idGood }, function (status) {
        $('.col-md-10-status').text(status);
    });


    $.get("/manage/GetBuyerOfGoodByIdBasket", { Id: idBasket }, function (lastname) {
        $('#a-buyer').text(lastname);
    });
})