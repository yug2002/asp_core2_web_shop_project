﻿$(document).ready(function () {
    var idCat = 0;
    var pageNumber = 0;
    var totalPages = 0;
    $('.a-categ').on('click', '.divCat', function () {
        idCat = +$(this).data('content');
        $.get('/home/GetGoodsByCategory', { id: idCat }, function (data) {
            $(".row-goods").empty();
            AppendGoods(data['goods']);

            var divPag = $("#div-wrap-pagination");
            divPag.empty();

            $(divPag).append('<div id="div-prev" class="paginCateg"></div>');
            $(divPag).append('<div id="div-curr"></div>');
            $(divPag).append('<div id="div-next" class="paginCateg"></div>');

            AppendPagination(data['pageViewModel']);     
        });
    });

    $('.col-goods').on('click', ".paginCateg", function () {
        var pageEnter = +$(this).text();
        if (pageEnter !== 0) {  
            $.get('/home/GetGoodsByCategory', { id: idCat, page: pageEnter }, function (data) {
                $(".row-goods").empty();
                AppendGoods(data['goods']);
                CorrectPagination(data['pageViewModel']);  
            });
        }
        return false;
    });
});



