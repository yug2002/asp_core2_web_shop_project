﻿$('#a-all-categ').on('click', '#all-categ', function () {
    var divRight = $("#right");
    var href = $(this).parent().attr('href');
    var coll = $('#' + href);
    var attr = coll.attr('aria-expanded');  
    if (attr === "true") {        
        coll.attr('aria-expanded', "false");
        $('#collapse-all-brands').attr('data-open', '0');
        $('#collapse-all-brands').animate({ height: '0' }, 500);
        setTimeout(function () { coll.removeClass('in'); },500);
    }
    else {        
        $.get('/home/GetBrands', null, function (brands) {
            
            coll.attr('aria-expanded', "true");
            coll.addClass('in');
            var sum = 0;
            var height = 0;
            var h = 0;
            $('#collapse-all-brands').empty();
            $('#collapse-all-brands').removeClass('collapse');
            $('#collapse-all-brands').addClass('collapsing');
            $('#collapse-all-brands').attr('data-open', '1');
            $(brands).each(function (i) {
                $('#collapse-all-brands').append('<div id="brand-' + i + '" class="panel-body"></div>');
                $('#brand-' + i).append('<a class="list-group-item" id="a-brand-' + i + '" href="#carousel" data-idCateg = "' + brands[i].categoryBrands[0].categoryId +'" data-idBrand="' + brands[i].id + '">' + brands[i].name + '</a>');
                if (i === 0) {
                    h = $('#brand-' + i).innerHeight();
                }                
                //----------------Анимированное открытие меню-------------//                
                sum = sum + h;
                $('#collapse-all-brands').animate({ height: sum + 'px' }, 40);
                //----------------------------------------------------------//
            });           
            $('#collapse-all-brands').removeClass('collapsing');
            $('#collapse-all-brands').addClass('collapse');

            return false;
        });
       
        $.get('/home/GetGoods', null, function (data) {
            divRight.empty();
            $(divRight).append('<div class="col-sm-12 row-goods"></div>');
            AppendGoods(data['goods']);

            //$(divRight).append('<div id="div-wrap-pagination"></div>');

            var divPag = $("#div-wrap-pagination");
            divPag.empty();

            $(divPag).append('<div id="div-prev" class="pagin"></div>');
            $(divPag).append('<div id="div-curr"></div>');
            $(divPag).append('<div id="div-next" class="pagin"></div>');

            AppendPagination(data['pageViewModel']);            
        });
    }
    var findDivs = $('.panel-heading').find('.divCat').find('span.glyphicon');
    var findColl = $('.panel-collapse');
    findColl.not('#collapse-all-brands').attr('aria-expanded', "false");
    findColl.not('#collapse-all-brands').removeClass('in');    
        
});