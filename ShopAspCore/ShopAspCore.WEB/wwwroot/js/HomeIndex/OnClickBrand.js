﻿$(document).ready(function () {

    var idB = 0;
    var idCateg = 0;
    var attr = "";
    $('.panel-collapse').on('click', '.list-group-item', function (event) { 

        idCateg = $(this).data('idcateg');        
        idB = $(this).data('idbrand');
        
        //------------------ анимация перехода к якорю -------------//
        event.preventDefault();
        var id = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({ scrollTop: top }, 1500);
        //------------------------------------------------------------//
        attr = $('#collapse-all-brands').attr('aria-expanded');
        if (attr === 'true') {
            $.get('/home/GetGoodsByBrandId', { idBrand: idB }, function (data) {

                GetGoods(data);
            });
        }
        else {
            $.get('/home/GetGoodsByBrandIdCategoryId', { idBrand: idB, idCategory: idCateg }, function (data) {                            
                GetGoods(data);               
            });
        }       

    });   

    $('.col-goods').on('click', ".paginBrand", function () {
        var pageEnter = +$(this).text();        
        if (pageEnter !== 0) {
            
            attr = $('#collapse-all-brands').attr('aria-expanded');
            if (attr === 'false') {
                $.get('/home/GetGoodsByBrandIdCategoryId', { idBrand: idB, idCategory: idCateg, page: pageEnter }, function (data) {

                    GetGoodsPagination(data);
                });
            }
            else {
                $.get('/home/GetGoodsByBrandId', { idBrand: idB, page: pageEnter }, function (data) {

                    GetGoodsPagination(data);
                });
            }  
        }
        return false;
    });

    var GetGoodsPagination = function getGoodsPagin(data) {
        $(".row-goods").empty();
        var rowGoods = $(".row-goods");
        AppendGoods(data['goods']);
        CorrectPagination(data['pageViewModel']);             
    };

    var GetGoods = function getGoods(data) {
        //var j = 0;
        var rowGoods = $(".row-goods");
        rowGoods.empty();
        
        AppendGoods(data['goods']);   

        var divPag = $("#div-wrap-pagination");
        divPag.empty();

        $(divPag).append('<div id="div-prev" class="paginBrand"></div>');
        $(divPag).append('<div id="div-curr"></div>');
        $(divPag).append('<div id="div-next" class="paginBrand"></div>');

        AppendPagination(data['pageViewModel']);            
    };
});