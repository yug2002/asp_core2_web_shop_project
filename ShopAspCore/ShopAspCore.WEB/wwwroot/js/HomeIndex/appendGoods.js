﻿function AppendGoods(item) {
    var j = 0;
    $(".row-goods").css("display", "none");
    $.each(item, function (i) {       

        $(".row-goods").append('<div id="' + j + '" class="row item-goods"></div>');
        var div_col = $('#' + j);
        $(div_col).append('<div  class="col-sm-3 charact img"><img src="' + item[i].imageB + '"/></div>');
        $(div_col).append('<div id="d' + j + '" class="col-md-offset-2 col-md-5 charact"></div>');

        var div_descr = $('#d' + j);
        div_descr.append('<div class="col-md-12 top-name">' + item[i].name + '</div>');
        div_descr.append('<div class="col-md-12 middle-code">Код товара: ' + item[i].code + '</div>');
        div_descr.append('<div class="col-md-12 bottom-descr">' + item[i].description + '</div>');

        $(div_col).append('<div id="p' + j + '" class="col-md-offset-2 col-md-4 charact purchase"></div>');
        var divPur = $('#p' + j);
        divPur.append('<div id="w' + j + '" class="wrap-purchase col-md-12"></div>');
        var divWrap = $('#w' + j);
        divWrap.append('<div class="price col-md-12">' + Math.round(item[i].price).toFixed(2) + ' руб.</div > ');
        divWrap.append('<div id="b' + j + '" class="buy col-md-12"></div>');
        var divBuy = $('#b' + j);
        divBuy.append('<button class="btn btn-success btn-buy" data-idGood="' + item[i].id +'" type="button"><span class="glyphicon glyphicon-shopping-cart"></span> ' + localization('Купить') + '</button > ');
        j++;
        //$(".row-goods").fadeIn(400);
    });
    $(".row-goods").fadeIn(400);
}
function GetCookie() {
    var c = '';
    c = $.cookie("login");    
    return c;
}

function GetCookieLang() {
    var culture = $.cookie("culture");
    return culture;
}
function AppendPagination(item) {
    
    var divPrev = $('#div-prev');
    var divCurr = $('#div-curr');
    var divNext = $('#div-next');

    $(divCurr).text(item.pageNumber);
    if (item.hasNextPage === true) {
        $(divNext).text(item.pageNumber + 1);
    }
    else {
        $(divNext).text('');
    }   
    
        return false;       
}

function CorrectPagination(item) {
    var divPrev = $('#div-prev');
    var divCurr = $('#div-curr');
    var divNext = $('#div-next');
    
    divPrev.text(item.pageNumber - 1);
    divCurr.text(item.pageNumber);
    divNext.text(item.pageNumber + 1);

    if (item.pageNumber === item.totalPages) {
        divNext.text('');
        return false;
    }
    else if (item.pageNumber === 1) {
        divPrev.text('');
        return false;
    }
    
}

var resourceRu_en = { 'Купить': 'Buy', 'Все категории': 'All categories' };
var resourceRu_be = { 'Купить': 'Купiць', 'Все категории': 'Усе катэгорыі' };
var resourceBe_en = { 'Купiць': 'Buy', 'Усе катэгорыі': 'All categories' };
var resourceBe_ru = { 'Купiць': 'Купить', 'Усе катэгорыі': 'Все категории' };
var resourceEn_ru = { 'Buy': 'Купить', 'All categories': 'Все категории' };
var resourceEn_be = { 'Buy': 'Купiць', 'All categories': 'Усе катэгорыі' };


function localization(word) {
    
    var lang = GetCookieLang();
    if (lang === 'ru') {

        var ru_be = resourceBe_ru[word];
        var ru_en = resourceEn_ru[word];
        var w_r = translate(ru_be, ru_en, word);
        return w_r;
    }
    else if (lang === 'en') {

        var en_b = resourceBe_en[word];
        var en_r = resourceRu_en[word];
        var w_e = translate(en_b, en_r, word);
        return w_e;
          
    }
    else if (lang === 'be') {
        var be_r = resourceEn_be[word];
        var be_e = resourceRu_be[word];
        var w_b = translate(be_r, be_e, word);
        return w_b;
    }
    else return word;
}

var translate = function (lang1, lang2, word) {
    var arr = [];
    var w = '';
    arr.push(lang1, lang2);
    for (var key in arr) {
        if (arr[key] !== undefined) {
            w = arr[key];
        }
    }
    if (w !== '') {
        return w;
    } else {
        return word;
    }
}





